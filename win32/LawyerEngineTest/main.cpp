#include <LawyerEngine/LawyerEngine.hpp>
#include "LawyerEngineGame.hpp"
#include "GameState.hpp"

#ifdef _WIN32
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>
#endif

int main(int argc, char* argv[]) {
  lwe::Log::getLogger().setLoggingLevel(lwe::LEVEL_DEBUG);

	lwe::GameSettings settings;
	settings.WindowTitle = "Lawyer Engine Test";

	int result;
	{
    lwe::GameEngine engine(settings, new LawyerEngineGame());
	  result = engine.run();
	}

#ifdef _WIN32
	_CrtDumpMemoryLeaks();
#endif

	return result;
}