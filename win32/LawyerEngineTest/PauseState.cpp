#include <LawyerEngine/LawyerEngine.hpp>
#include "PauseState.hpp"

PauseState::PauseState() : lwe::State()
{
}


PauseState::~PauseState()
{
}

bool PauseState::init()
{
	LOG_DEBUG("Init PauseState");
	return true;
}

void PauseState::cleanup()
{
	LOG_DEBUG("Cleanup PauseState");
}

void PauseState::onEnter()
{
	LOG_DEBUG("Enter PauseState");
}

void PauseState::onExit()
{
	LOG_DEBUG("Exit PauseState");
}

bool PauseState::handleEvent(const SDL_Event& ev)
{
	if (ev.type == SDL_KEYDOWN)
	{
		if (ev.key.keysym.sym == SDLK_SPACE)
		{
			LOG_DEBUG("Pop PauseState with cleanup");
			getEngine()->popState(true);
			return true;
		}
	}
	return false;
}
