#ifndef PAUSESTATE_H
#define PAUSESTATE_H

#include <LawyerEngine/LawyerEngine.hpp>

class PauseState : public lwe::State
{
public:
	PauseState();
	~PauseState();

	bool init() override;
	void cleanup() override;
	
	void onEnter() override;
	void onExit() override;

	bool handleEvent(const SDL_Event& ev) override;
};

#endif