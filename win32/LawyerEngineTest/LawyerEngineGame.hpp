#ifndef LAWYERENGINEGAME_H
#define LAWYERENGINEGAME_H

#include <LawyerEngine/LawyerEngine.hpp>
#include "GameState.hpp"
#include "PauseState.hpp"

class LawyerEngineGame : public lwe::Game
{
public:
	LawyerEngineGame();
	virtual ~LawyerEngineGame();

	void init() override;

	PauseState* const getPauseState();
	GameState* const getGameState();

private:
	GameState* gameState { nullptr };
	PauseState* pauseState { nullptr };
};

#endif