#include "LawyerEngineGame.hpp"


LawyerEngineGame::LawyerEngineGame()
{
}


LawyerEngineGame::~LawyerEngineGame()
{
}

void LawyerEngineGame::init() {
	gameState = new GameState();
	getEngine()->setState(gameState);
}

GameState* const LawyerEngineGame::getGameState()
{
	return gameState;
}

PauseState* const LawyerEngineGame::getPauseState()
{
	if (pauseState == nullptr)
	{
		pauseState = new PauseState();
	}

	return pauseState;
}