#include <LawyerEngine/LawyerEngine.hpp>
#include "LawyerEngineGame.hpp"
#include "GameState.hpp"

bool GameState::init()
{
	LOG_DEBUG("Init GameState");

	// load assets managed by assetmanager

	lwe::AssetManager* am = getEngine()->getAssetManager();
	am->load<lwe::Texture>("wood.png");
	am->load<lwe::TextureAtlas>("spritesheet.xml");
  am->load<lwe::Skin>("skinFile.xml");

	wood = am->get<lwe::Texture>("wood.png");
	atlas = am->get<lwe::TextureAtlas>("spritesheet.xml");
  lwe::Skin& skin = *am->get<lwe::Skin>("skinFile.xml");

  std::shared_ptr<lwe::Button> button = std::make_shared<lwe::Button>(skin, 50.f, 50.f);
  button->setAction([](lwe::Button*)
  {
    LOG_INFO("Button action performed.");
  });	
  button->setCheckable(true);
  gui.addWidget(button);

  std::shared_ptr<lwe::Button> disabledButton = std::make_shared<lwe::Button>(skin, 50.f, 100.f);
  disabledButton->setEnabled(false);
  gui.addWidget(disabledButton);

  std::vector<lwe::TextureRegion*> regions = atlas->findRegions("simple-button");
  std::vector<std::shared_ptr<lwe::Button>> buttonGroup;
  buttonGroup.push_back(std::make_shared<lwe::Button>(regions[0], regions[1], regions[2], 200.f, 50.f));
  buttonGroup.back()->setAction([](lwe::Button*)
  {
    LOG_INFO("Button 1 pressed.");
  });
  buttonGroup.push_back(std::make_shared<lwe::Button>(regions[0], regions[1], regions[2], 200.f, 100.f));
  buttonGroup.back()->setAction([](lwe::Button*)
  {
    LOG_INFO("Button 2 pressed.");
  });
  buttonGroup.push_back(std::make_shared<lwe::Button>(regions[0], regions[1], regions[2], 200.f, 150.f));
  buttonGroup.back()->setAction([](lwe::Button*)
  {
    LOG_INFO("Button 3 pressed.");
  });

  buttonGroup[0]->setSelected(true);
  lwe::Button::createVerticalButtonGroup({ buttonGroup[0], buttonGroup[1], buttonGroup[2] });

  gui.addWidget(buttonGroup[0]);
  gui.addWidget(buttonGroup[1]);
  gui.addWidget(buttonGroup[2]);
  
  buttonGroup[0]->setWest(button);
  buttonGroup[1]->setWest(button);
  buttonGroup[2]->setWest(button);
  button->setEast(buttonGroup[0]);
  button->setSouth(disabledButton);
  disabledButton->setNorth(button);

	//////////////////////////////////////
	
	// Load regular managed assets

	font = new lwe::Font();
	font->loadFromFile("VeraMono.ttf");
	
	text = new lwe::Text(getEngine()->getRenderer(), font, "Some text", 12, 300, 200, 255, 255, 255);

	wood2 = lwe::Texture::createTexture(getEngine()->getRenderer(), "wood.png");
	
	atlas2 = new lwe::TextureAtlas(getEngine()->getRenderer(), "spritesheet.xml");
	if ((atlasCar2_1 = atlas2->findRegion("car1")) == NULL) return false;
	if ((atlasCar2_2 = atlas2->findRegion("car2")) == NULL) return false;

	//////////////////////////////////////


	return true;
}

void GameState::cleanup()
{
	LOG_DEBUG("Cleanup GameState");
	delete atlas2;
	delete wood2;
	delete font;
	delete text;
}

bool GameState::handleEvent(const SDL_Event& ev)
{
  if (gui.handleEvent(ev))
  {
    return true;
  }

  if (ev.type == SDL_KEYDOWN)
	{
		if (ev.key.keysym.sym == SDLK_k)
		{
			lwe::GameSettings* s = getSettings();
			s->EntityInterpolationEnabled = !s->EntityInterpolationEnabled;
      LOG_INFO("Interpolation is now %s", (s->EntityInterpolationEnabled ? "enabled." : "disabled."));
			return true;
		}

		if (ev.key.keysym.sym == SDLK_ESCAPE)
		{
			LOG_DEBUG("Push PauseState");
			LawyerEngineGame* game = getGame<LawyerEngineGame>();
			getEngine()->pushState(game->getPauseState());
		}
	}

	return false;
}

void GameState::render(SDL_Renderer* const renderer, const float timeAlpha)
{
	renderAssetManagerAssets(renderer, timeAlpha);
	renderNormalAssets(renderer, timeAlpha);
}

void GameState::renderAssetManagerAssets(SDL_Renderer* const renderer, const float timeAlpha)
{
	lwe::GameEngine* engine = getEngine();
	int windowWidth = engine->getWindowWidth();
	int windowHeight = engine->getWindowHeight();

	SDL_Rect topLeft = { 0, 0, wood->getWidth(), wood->getHeight() };

	SDL_RenderCopy(renderer, wood->getSDLTexture(), NULL, &topLeft);

  gui.render(renderer);
}

void GameState::renderNormalAssets(SDL_Renderer* const renderer, const float timeAlpha)
{
	lwe::GameEngine* engine = getEngine();
	int windowWidth = engine->getWindowWidth();
	int windowHeight = engine->getWindowHeight();

	SDL_Rect middle = { 
		(int)(windowWidth / 2.f), (int)(windowHeight / 2.f),
		atlasCar2_1->getRegionWidth(), atlasCar2_1->getRegionHeight() 
	};

	SDL_RenderCopy(renderer, atlasCar2_1->getSDLTexture(), &atlasCar2_1->getRect(), &middle);

	text->render();
}