#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <LawyerEngine/LawyerEngine.hpp>

class GameState : public lwe::State
{
public:
	GameState() {}
	~GameState() {}

	bool init() override;
	void cleanup() override;
	
	bool handleEvent(const SDL_Event& ev) override;
	void render(SDL_Renderer* renderer, float timeAlpha) override;

private:
	void renderAssetManagerAssets(SDL_Renderer* renderer, float timeAlpha);
	void renderNormalAssets(SDL_Renderer* renderer, float timeAlpha);

	// managed by assetmanager
	lwe::TextureAtlas* atlas{ NULL };
	lwe::Texture* wood{ NULL };

	// managed manually
	lwe::Texture* wood2{ NULL };
	lwe::TextureRegion* atlasCar2_1{ NULL };
	lwe::TextureRegion* atlasCar2_2{ NULL };
	lwe::TextureAtlas* atlas2{ NULL };

	lwe::Font* font{ NULL };
	lwe::Text* text{ NULL };

  lwe::GUI gui;
};

#endif