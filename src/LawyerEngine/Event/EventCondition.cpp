#include <LawyerEngine/Event/EventCondition.hpp>

namespace lwe
{

EventCondition::EventCondition()
{
}

EventCondition::~EventCondition()
{
}

bool EventCondition::operator()(const SDL_Event& ev) const
{
  bool consume = false;
  return operator()(ev, &consume);
}

bool EventCondition::operator()(const SDL_Event& ev, bool* result) const
{
  for (const std::shared_ptr<Trigger>& trigger : triggers)
  {
    if ((*trigger)(ev, result))
    {
      return true;
    }
  }

  return false;
}

void EventCondition::addTrigger(const std::shared_ptr<Trigger>& trigger)
{
  triggers.push_back(trigger);
}

bool EventCondition::hasTriggers() const
{
  return !triggers.empty();
}

} // namespace lwe