#include <LawyerEngine/Event/Trigger.hpp>

namespace lwe
{

KeyboardTrigger::KeyboardTrigger(const SDL_Keycode key)
  : KeyboardTrigger(key, false)
{
}

KeyboardTrigger::KeyboardTrigger(const SDL_Keycode key, const bool onDown)
  : onDown(onDown), key(key), pressed(false)
{
}

KeyboardTrigger::~KeyboardTrigger()
{
}

bool KeyboardTrigger::operator()(const SDL_Event& ev, bool* result)
{
  if (onDown)
  {
    if (ev.type == SDL_KEYDOWN)
    {
      if (ev.key.keysym.sym == key)
      {
        (*result) = true;
        return true;
      }
    }

    return false;
  }

  if (ev.type == SDL_KEYDOWN)
  {
    if (ev.key.keysym.sym == key)
    {
      pressed = true;
      return true;
    }
  }

  if (ev.type == SDL_KEYUP && pressed)
  {
    if (ev.key.keysym.sym == key)
    {
      pressed = false;
      (*result) = true;
      return true;
    }
  }

  return false;
}

} // namespace lwe