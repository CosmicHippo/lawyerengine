#include <LawyerEngine/Event/Trigger.hpp>

namespace lwe
{

GameControllerButtonTrigger::GameControllerButtonTrigger(SDL_GameControllerButton button)
  : button(button)
{
}

GameControllerButtonTrigger::GameControllerButtonTrigger(SDL_GameControllerButton button, bool onDown)
  : button(button), onDown(onDown)
{
}

GameControllerButtonTrigger::GameControllerButtonTrigger(Sint32 device, SDL_GameControllerButton button)
  : device(device), button(button)
{
}

GameControllerButtonTrigger::GameControllerButtonTrigger(Sint32 device, SDL_GameControllerButton button, bool onDown)
  : device(device), button(button), onDown(onDown)
{
}

GameControllerButtonTrigger::~GameControllerButtonTrigger()
{
}

bool GameControllerButtonTrigger::isCorrectDevice(Sint32 device) const
{
  return (this->device <  0 || this->device == device);
}

bool GameControllerButtonTrigger::operator()(const SDL_Event& ev, bool* result)
{
  if (onDown)
  {
    if (ev.type == SDL_CONTROLLERBUTTONDOWN && isCorrectDevice(ev.cdevice.which))
    {
      if (ev.cbutton.button == button)
      {
        (*result) = true;
        return true;
      }
    }

    return false;
  }

  if (ev.type == SDL_CONTROLLERBUTTONDOWN && isCorrectDevice(ev.cdevice.which))
  {
    if (ev.cbutton.button == button)
    {
      pressed = true;
      return true;
    }
  }
  else if (ev.type == SDL_CONTROLLERBUTTONUP && isCorrectDevice(ev.cdevice.which) && pressed)
  {
    if (ev.cbutton.button == button)
    {
      pressed = false;
      (*result) = true;
      return true;
    }
  }

  return false;
}

} // namespace lwe