#include <LawyerEngine/Utils/Utils.hpp>

#include <SDL.h>

namespace lwe
{

namespace Utils
{

/**
 http://stackoverflow.com/questions/1855884/determine-font-color-based-on-background-color/1855903#1855903
 */
SDL_Color getContrastColor(const SDL_Color& color)
{
	Uint8 d = 0;

    // Counting the perceptive luminance - human eye favors green color... 
    double a = 1 - ( 0.299 * color.r + 0.587 * color.g + 0.114 * color.b) / 255.0;

    if (a < 0.5)
    {
       d = 0; // bright colors - black font
    }
    else
    {
       d = 255; // dark colors - white font
    }

    return { d, d, d, 255};
}

} // namespace lwe::utils

} // namespace lwe