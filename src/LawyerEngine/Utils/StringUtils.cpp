#include <LawyerEngine/Utils/StringUtils.hpp>

namespace lwe
{

namespace Utils
{

bool endsWith(const std::string& string, const std::string& suffix)
{
  if (string.length() >= suffix.length())
  {
    return (0 == string.compare(string.length() - suffix.length(), suffix.length(), suffix));
  }

  return false;
}

bool startsWith(const std::string& string, const std::string& start)
{
  if (string.size() >= start.size())
  {
    return string.find(start) == 0;
  }

  return false;
}

std::string nullToEmpty(const char* const str)
{
  if (str == nullptr)
  {
    return "";
  }

  return std::string(str);
}

std::string getFileExtension(const std::string& fileName)
{
  auto extensionPos = fileName.rfind(".");
  if (extensionPos != std::string::npos)
  {
    return fileName.substr(extensionPos);
  }

  return "";
}

std::string getFileWithoutExtension(const std::string& fileName)
{
  const std::string& extension = getFileExtension(fileName);
  if (extension != "")
  {
    return fileName.substr(0, fileName.rfind(extension));
  }
  else
  {
    return fileName;
  }
}

}

}