#include <LawyerEngine/Utils/Log.hpp>
#include <SDL.h>

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <Windows.h>
#include <sstream>
#include <string>
#include <cstdio>
#endif

#include <iostream>

namespace lwe
{

Log Log::logger;

struct UserData
{
  const char* func;
  int line;
};

const char* toString(LoggingLevel level)
{
  static const char* const buffer[] = { "TRACE", "DEBUG", "INFO", "WARN", "ERROR" };
  {
    return buffer[level - 1];
  }
}

std::string nowTime()
{
  const int MAX_LEN = 200;
  char buffer[MAX_LEN];
  if (GetTimeFormatA(LOCALE_USER_DEFAULT, 0, 0,
    "HH':'mm':'ss", buffer, MAX_LEN) == 0)
    return "Error in nowTime()";

  char result[100] = { 0 };
  static DWORD first = GetTickCount();
  sprintf_s(result, "%s.%03ld", buffer, (long)(GetTickCount() - first) % 1000);
  return result;
}

SDL_LogPriority toSDL_LogPriority(LoggingLevel level)
{
  switch (level)
  {
  case lwe::LEVEL_TRACE:
    return SDL_LOG_PRIORITY_VERBOSE;
  case lwe::LEVEL_DEBUG:
    return SDL_LOG_PRIORITY_DEBUG;
  case lwe::LEVEL_INFO:
    return SDL_LOG_PRIORITY_INFO;
  case lwe::LEVEL_WARN:
    return SDL_LOG_PRIORITY_WARN;
  case lwe::LEVEL_ERROR:
    return SDL_LOG_PRIORITY_ERROR;
  default:
    return SDL_LOG_PRIORITY_INFO;
  }
}

LoggingLevel toLoggingLevel(SDL_LogPriority priority)
{
  switch (priority)
  {
  case SDL_LOG_PRIORITY_VERBOSE:
    return lwe::LEVEL_TRACE;
  case SDL_LOG_PRIORITY_DEBUG:
    return lwe::LEVEL_DEBUG;
  case SDL_LOG_PRIORITY_INFO:
    return lwe::LEVEL_INFO;
  case SDL_LOG_PRIORITY_WARN:
    return lwe::LEVEL_WARN;
  case SDL_LOG_PRIORITY_ERROR:
  case SDL_LOG_PRIORITY_CRITICAL:
    return lwe::LEVEL_ERROR;
  default:
    return lwe::LEVEL_INFO;
  }
}

void logMessage(LoggingLevel level, const char* func, int line, const char* fmt, va_list ap)
{
  char* message = new char[4096];
  size_t len;

  if (!message)
  {
    return;
  }

  vsnprintf_s(message, 4096, 4096, fmt, ap);

  len = std::strlen(message);
  if (len > 0 && message[len - 1] == '\n')
  {
    message[--len] = '\0';
    if (len > 0 && message[len - 1] == '\r')
    {
      message[--len] = '\0';
    }
  }

  char* finalMsg = new char[4096];
  if (std::strlen(func) > 0 && line >= 0)
  {
    sprintf_s(finalMsg, 4096, "%s %s %s@%d: %s\r\n", nowTime().c_str(), toString(level), func, line, message);
  }
  else
  {
    sprintf_s(finalMsg, 4096, "%s %s: %s\r\n", nowTime().c_str(), toString(level), message);
  }

  OutputDebugStringA(finalMsg);
  std::cout << finalMsg;

  delete[] message;
  delete[] finalMsg;
}

void callback(void *userdata, int category, SDL_LogPriority priority, const char *message)
{
  va_list ap;
  va_start(ap, message);
  logMessage(toLoggingLevel(priority), "", -1, message, ap);
  va_end(ap);
}

Log::Log()
{
  SDL_LogSetAllPriority(toSDL_LogPriority(getLoggingLevel()));
  SDL_LogSetOutputFunction(&callback, nullptr);
}

Log& Log::getLogger()
{
  return Log::logger;
}

void Log::setLoggingLevel(LoggingLevel level)
{
  SDL_LogSetAllPriority(toSDL_LogPriority(level));
  currentLevel = level;
}

void Log::log(LoggingLevel level, const char* func, int line, const char* msg, ...)
{
  va_list ap;
  va_start(ap, msg);
  logMessage(level, func, line, msg, ap);
  va_end(ap);
}

} // lwe