#include <LawyerEngine/Graphics/Texture.hpp>
#include <LawyerEngine/Utils/Log.hpp>
#include <SDL_image.h>

namespace lwe {

Texture::Texture(SDL_Texture* const texture)
  : texture(texture)
{
  SDL_QueryTexture(texture, &format, &access, &width, &height);
}

Texture::~Texture()
{
  SDL_DestroyTexture(texture);
}

Texture* Texture::createTexture(SDL_Renderer* const renderer, const std::string& fileName)
{
  SDL_Surface* const surface = IMG_Load(fileName.c_str());
  if (surface == nullptr)
  {
    LOG_ERROR("Couldn't load texture %s", fileName.c_str());
    return NULL;
  }

  SDL_Texture* const SDLTexture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);

  if (SDLTexture == nullptr)
  {
    LOG_ERROR("Couldn't create texture from loaded file: %s", fileName.c_str());
    return NULL;
  }

  LOG_DEBUG("Texture %s loaded successfully.", fileName.c_str());
  return new Texture(SDLTexture);
}

int Texture::getWidth() const
{
  return width;
}

int Texture::getHeight() const
{
  return height;
}

int Texture::getAccess() const
{
  return access;
}

Uint32 Texture::getFormat() const
{
  return format;
}

SDL_Texture* Texture::getSDLTexture() const
{
  return texture;
}

} // namespace lwe