#include <LawyerEngine/Graphics/Text.hpp>
#include <LawyerEngine/Utils/Log.hpp>
#include <iostream>
#include <sstream>
#include <list>

namespace lwe {

Text::Text(SDL_Renderer* renderer, Font* font, std::string text, const int fontSize, const float x, const float y, const SDL_Color& color)
  : Text(renderer, font, text, fontSize, x, y, color.r, color.g, color.b)
{
}

Text::Text(SDL_Renderer* renderer, Font* font, std::string _text, const int _fontSize, const float _x, const float _y, int r, int g, int b)
  : m_fontSize(_fontSize), m_renderer(renderer), m_font(font), m_x(_x), m_y(_y), m_text(_text)
{
  m_color.r = r;
  m_color.g = g;
  m_color.b = b;

  ensureTextUpdated();
}

Text::~Text()
{
  delete m_texture;
}

void Text::render()
{
  if(!isVisible())
  {
    return;
  }

  ensureTextUpdated();

  if (m_texture == NULL)
  {
    return;
  }

  SDL_Rect destRect;
  destRect.x = (int)getX();
  destRect.y = (int)getY();
  destRect.w = (int)getWidth();
  destRect.h = (int)getHeight();

  SDL_RenderCopy(m_renderer, m_texture->getSDLTexture(), NULL, &destRect);
}

void Text::ensureTextUpdated()
{
  if (!needsUpdate)
  {
    return;
  }

  TTF_Font* const font = m_font->getTTF_Font(m_fontSize);

  SDL_Surface* surface = TTF_RenderText_Blended(font, m_text.c_str(), m_color);
  if (surface != nullptr)
  {
    SDL_Texture* sdl_texture = SDL_CreateTextureFromSurface(m_renderer, surface);
    SDL_FreeSurface(surface);
    surface = nullptr;

    if (sdl_texture != nullptr)
    {
      delete m_texture;
      m_texture = new Texture(sdl_texture);
      m_width = (float)m_texture->getWidth();
      m_height = (float)m_texture->getHeight();
    }
  }
  else
  {
    LOG_ERROR("Creating text with font %s(%i) with text %s failed.", m_font->getFileName().c_str(), m_fontSize, m_text.c_str());
  }

  needsUpdate = false;
}

void Text::updateText(const std::string& newText)
{
  if (m_text != newText)
  {
    needsUpdate = true;
    m_text = newText;
  }
}

void Text::updateText(const int _number)
{
  updateText(std::to_string(_number));
}

const SDL_Color& Text::getColor() const
{
  return m_color;
}

void Text::setColor(const SDL_Color& color)
{
  m_color.r = color.r;
  m_color.g = color.g;
  m_color.b = color.b;
}

void Text::setColor(int r, int g, int b)
{
  m_color.r = r;
  m_color.g = g;
  m_color.b = b;
}

void Text::setColor(float r, float g, float b)
{
  
  // TODO clamp input values
  m_color.r = (Uint8)(r * 255.f);
  m_color.g = (Uint8)(g * 255.f);
  m_color.b = (Uint8)(b * 255.f);
}


float Text::getX() const
{
  return m_x;
}
float Text::getY() const
{
  return m_y;
}
void Text::setX(const float x)
{
  m_x = x;
}
void Text::setY(const float y)
{
  m_y = y;
}

float Text::getWidth() const
{
  return m_width;
}
float Text::getHeight() const
{
  return m_height;
}

bool Text::isVisible() const
{
  return m_visible;
}
void Text::setVisible(const bool visible)
{
  m_visible = visible;
}

int Text::getFontSize() const
{
  return m_fontSize;
}

void Text::setFontSize(const int size)
{
  m_fontSize = size;
}

} // namespace lwe