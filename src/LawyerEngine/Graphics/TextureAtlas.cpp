#include <LawyerEngine/Graphics/TextureAtlas.hpp>
#include <LawyerEngine/Utils/Filesystem.hpp>
#include <LawyerEngine/Utils/Log.hpp>
#include <LawyerEngine/Utils/Utils.hpp>

#include <SDL.h>
#include <SDL_image.h>
#include <TinyXML/tinyxml2.h>

namespace lwe {

AtlasRegion::AtlasRegion(const std::string& name, int index, Texture* t, int x, int y, int w, int h) 
  : TextureRegion(t, x, y, w, h), name(name), index(index)
{
}

AtlasRegion::~AtlasRegion()
{
  LOG_DEBUG("Destroying atlas region [%s] - Index: [%i]", name.c_str(), index);
}

std::string AtlasRegion::getName() const
{
  return name;
}

TextureAtlas::TextureAtlas(SDL_Renderer* renderer, const std::string& _file) : renderer(renderer), m_fileName(_file)
{
  load();
}

TextureAtlas::~TextureAtlas()
{
  LOG_DEBUG("Deleting texture atlas %s", m_fileName.c_str());
  for (Texture*& texture : textures)
  {
    delete texture;
  }

  for (AtlasRegion*& region : regions)
  {
    delete region;
  }
}

void TextureAtlas::load()
{
  std::vector<std::string> files;

  // Are we dealing with a multipage spritesheet?

  std::string fileNameWithoutExtension = Utils::getFileWithoutExtension(m_fileName);
  if (fileNameWithoutExtension.rfind("_") != std::string::npos)
  {
    std::string fileNameWithoutUnderscore = fileNameWithoutExtension.substr(0, fileNameWithoutExtension.rfind("_"));
    LOG_INFO("Loading multipage atlas: %s", m_fileName.c_str());
    int i = 0;
    while (true)
    {
      std::string indexS = std::to_string(i);
      std::string fileName = fileNameWithoutUnderscore + "_" + indexS + ".xml";
      LOG_DEBUG("Checking for file %s", fileName.c_str());

      if (!filesys::fileExists(fileName))
      {
        break;
      }

      files.push_back(fileName);
      i++;
    }
  }
  else
  {
    if (!filesys::fileExists(m_fileName))
    {
      LOG_ERROR("FILE not found: %s", m_fileName.c_str());
      return;
    }

    files.push_back(m_fileName);
    LOG_INFO("Loading single page atlas from %s", m_fileName.c_str());
  }

  if (files.empty())
  {
    LOG_INFO("No atlas files found. %s", m_fileName.c_str());
    return;
  }

  int pageNumber = 0;
  for (std::string fileName : files)
  {
    tinyxml2::XMLDocument doc;
    if (doc.LoadFile(fileName.c_str()))
    {
      LOG_ERROR("Couldn't load atlas file %s", fileName.c_str());
    }
    else
    {
      tinyxml2::XMLElement* root = doc.FirstChildElement();

      std::string currPath = fileName.substr(0, fileName.rfind("/") + 1);
      std::string textureFileName = currPath + std::string(root->Attribute("imagePath"));

      LOG_DEBUG("Loading texture file %s", textureFileName.c_str());
      SDL_Surface* loadedSurface = IMG_Load(textureFileName.c_str());
      if (loadedSurface == nullptr)
      {
        LOG_ERROR("Couldn't load texture %s", textureFileName.c_str());
        continue;
      }

      SDL_Texture* newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
      if (newTexture == NULL)
      {
        LOG_ERROR("Couldn't create texture from loaded file: %s", textureFileName.c_str());
        continue;
      }
      SDL_FreeSurface(loadedSurface);

      Texture* texture = new Texture(newTexture);
      LOG_DEBUG("Texture %s loaded successfully.", textureFileName.c_str());

      textures.push_back(texture);

      // find all texture regions
      tinyxml2::XMLElement* sprite = root->FirstChildElement("sprite");

      while (sprite != NULL)
      {
        int x, y, w, h;
        x = y = w = h = -1;
        sprite->QueryIntAttribute("x", &x);
        sprite->QueryIntAttribute("y", &y);
        sprite->QueryIntAttribute("w", &w);
        sprite->QueryIntAttribute("h", &h);
        const char* nameStr = sprite->Attribute("n");
        std::string name = nameStr != NULL ? nameStr : "";

        // name.length must be 5 or more to be able to fit ".png" extension
        if (x < 0 || y < 0 || w < 0 || h < 0 || name.length() < 5)
        {
          LOG_ERROR("Error reading sprite in XML");
        }
        else
        {
          name.erase(name.length() - 4, 4); // strip ".png"
          // parse the end and see if it ends with "_<number>"
          // if so, set the number to be the index
          int index = 0;
          int i = name.find_last_of('_');
          if (i != std::string::npos && i != name.size() - 1) {
            
            
            std::string numberPart = name.substr(i + 1, name.length() - i);
            int number = atoi(numberPart.c_str());
            index = number;
            if (index < 0) index = 0;

            int numberCount = name.size() - 1 - i;
            name.erase(i, numberCount + 1);
          }

          regions.push_back(new AtlasRegion(name, index, texture, x, y, w, h));
        }
        sprite = sprite->NextSiblingElement("sprite");
      }

    }
    pageNumber++;
  }
}

TextureRegion* TextureAtlas::findRegion(const std::string& name) const
{
  for (auto& ptr : regions)
  {
    if (ptr->name == name)
    {
      return ptr;
    }
  }

  return nullptr;
}

TextureRegion* TextureAtlas::findRegion(const std::string& name, int index) const
{
  for (AtlasRegion* const& ptr : regions)
  {
    if (ptr->index == index && ptr->name == name)
    {
      return ptr;
    }
  }

  return nullptr;
}

std::vector<TextureRegion*> TextureAtlas::findRegions(const std::string& name) const
{
  std::vector<TextureRegion*> matched;
  for (AtlasRegion* const& ptr : regions)
  {
    if (ptr->name == name)
    {
      matched.push_back(ptr);
    }
  }

  return matched;
}

} // namespace lwe