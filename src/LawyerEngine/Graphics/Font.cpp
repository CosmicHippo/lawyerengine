#include <LawyerEngine/Graphics/Font.hpp>

namespace lwe {

Font::~Font()
{
  for (auto& pair : m_fonts)
  {
    TTF_CloseFont(pair.second);
  }
}

bool Font::loadFromFile(std::string fileName)
{
  if (loaded)
  {
    return false;
  }
  loaded = true;

  m_fileName = fileName;

  TTF_Font* font = TTF_OpenFont(fileName.c_str(), 12);
  if (font == NULL)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Font could not be loaded. %s", fileName.c_str());
    return false;
  }
  m_fonts[12] = font;
  return true;
}

TTF_Font* Font::loadSize(int size)
{
  const auto& font = TTF_OpenFont(m_fileName.c_str(), size);
  if (font == NULL)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Font could not be loaded. %s", m_fileName.c_str());
    return false;
  }
  m_fonts[size] = font;
  return font;
}

TTF_Font* Font::getTTF_Font(int size)
{
  TTF_Font* f = m_fonts[size];
  if (f == NULL)
  {
    f = loadSize(size);
    if (f == NULL)
    {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Font: size %s could not be loaded. %s", size, m_fileName.c_str());
      return NULL;
    }
  }
  return f;
}

std::string Font::getFileName() const
{
  return m_fileName;
}

} // namespace lwe