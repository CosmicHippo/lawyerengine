#include <LawyerEngine/Graphics/TextureRegion.hpp>

namespace lwe {

TextureRegion::TextureRegion()
{
  setRegion(0, 0, 0, 0);
}

TextureRegion::TextureRegion(TextureRegion const& region) 
  : texture(region.texture) 
{
  setRegion(region.regionX, region.regionY, region.regionWidth, region.regionHeight);
}

TextureRegion::TextureRegion(Texture* const texture) 
  : TextureRegion(texture, texture->getWidth(), texture->getHeight()) 
{
}

TextureRegion::TextureRegion(Texture* const texture, int width, int height) 
  : TextureRegion(texture, 0, 0, width, height) 
{
}

TextureRegion::TextureRegion(Texture* const texture, int x, int y, int width, int height) 
  : texture(texture) 
{
  setRegion(x, y, width, height);
}

TextureRegion::~TextureRegion()
{
}

void TextureRegion::setRegion(Texture* const texture)
{
  setRegion(0, 0, texture->getWidth(), texture->getHeight());
}

void TextureRegion::setRegion(TextureRegion* const textureRegion)
{
  setRegion(textureRegion->regionX, textureRegion->regionY, textureRegion->regionWidth, textureRegion->regionHeight);
}

void TextureRegion::setRegion(TextureRegion const& textureRegion)
{
  setRegion(textureRegion.regionX, textureRegion.regionY, textureRegion.regionWidth, textureRegion.regionHeight);
}

void TextureRegion::setRegion(int x, int y, int width, int height)
{
  regionX = x;
  regionY = y;
  regionWidth = width;
  regionHeight = height;
  regionRect = SDL_Rect{ regionX, regionY, regionWidth, regionHeight };
}

int TextureRegion::getRegionX() const
{
  return regionX;
}

int TextureRegion::getRegionY() const
{
  return regionY;
}

int TextureRegion::getRegionWidth() const
{
  return regionWidth;
}

int TextureRegion::getRegionHeight() const
{
  return regionHeight;
}

const SDL_Rect& TextureRegion::getRect()
{
  return regionRect;
}

Texture* TextureRegion::getTexture() const
{
  return texture;
}
SDL_Texture* TextureRegion::getSDLTexture() const
{
  return texture->getSDLTexture();
}


} // namespace lwe