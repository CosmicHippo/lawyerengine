#include <LawyerEngine/UI/Skin.hpp>
#include <LawyerEngine/UI/Button.hpp>
#include <LawyerEngine/UI/TextButton.hpp>
#include <LawyerEngine/Utils/Log.hpp>
#include <LawyerEngine/Utils/Utils.hpp>
#include <TinyXML/tinyxml2.h>

#include <functional>

namespace lwe
{

Skin::Skin(SDL_Renderer* const renderer, const std::string& skinFile)
  : renderer(renderer)
{
  parseSkinFile(skinFile);
}

Skin::~Skin()
{
}

bool contains(std::string values[], std::string value)
{
  for (int i = 0; i < sizeof(values); i++)
  {
    if (value == values[i])
    {
      return true;
    }
  }

  return false;
}

void assignButtonStyle(const tinyxml2::XMLElement* const style, const TextureAtlas* atlas, TextureRegion*& region)
{
  const std::string name = style->Name();
  const char* indexStr = style->Attribute("index");
  const int index = indexStr != nullptr ? atoi(indexStr) : -1;

  if (style->GetText() == nullptr)
  {
    LOG_ERROR("Empty text in tag %s", style->Name());
    return;
  }

  const std::string text(style->GetText());
  LOG_DEBUG("Style %s = %s", name.c_str(), text.c_str());
  if (index < 0)
  {
    region = atlas->findRegion(text);
  }
  else
  {
    std::vector<TextureRegion*> regions = atlas->findRegions(text);
    if (index < (int)regions.size())
    {
      region = regions[index];
    }
    else
    {
      LOG_ERROR("Index out of bounds for Style: %s Region: %s", name.c_str(), text.c_str());
    }
  }

  if (region == nullptr)
  {
    LOG_ERROR("Style %s not found in atlas: %s", name.c_str(), text.c_str());
  }
}

std::unique_ptr<TextButtonStyle> Skin::parseTextButtonStyle(Skin* const skin, tinyxml2::XMLElement* element)
{
  std::unique_ptr<TextButtonStyle> textButtonStyle = std::make_unique<TextButtonStyle>();
  textButtonStyle->name = element->Attribute("name");

  ButtonStyle* buttonStyle;
  if (element->Attribute("extends") != nullptr)
  {
    std::string extends = element->Attribute("extends");
    std::vector<std::string> split = Utils::split(extends, ':');
    if (split.size() != 2 && split[0] != "button")
    {
      LOG_ERROR("Unsupported extension: %s", extends.c_str());
      return textButtonStyle;
    }

    std::string& extendedButton = split[1];
    buttonStyle = skin->getStyle<ButtonStyle>(extendedButton);
  }
  else
  {
    buttonStyle = parseButtonStyle(skin, element).get();
  }
  textButtonStyle->up = buttonStyle->up;
  textButtonStyle->over = buttonStyle->over;
  textButtonStyle->down = buttonStyle->down;
  textButtonStyle->checked = buttonStyle->checked;
  textButtonStyle->disabled = buttonStyle->disabled;

  for (tinyxml2::XMLElement* childElement = element->FirstChildElement();
       childElement != nullptr;
       childElement = childElement->NextSiblingElement())
  {
    std::string name = childElement->Name();
    if (name == "font")
    {
      std::string fontRef = childElement->GetText();
      try
      {
        std::size_t index = skin->fontMapping.at(fontRef);
        std::unique_ptr<Font>& font = skin->fonts[index];
        textButtonStyle->font = font.get();
      }
      catch (std::out_of_range&)
      {
        LOG_ERROR("Font does not exist. %s", fontRef.c_str());
      }
    }
    else if (name == "color")
    {
      std::string colorRef = childElement->GetText();
      try
      {
        std::size_t index = skin->colorMapping.at(colorRef);
        SDL_Color& color = skin->colors[index];
        textButtonStyle->color = color;
      }
      catch (std::out_of_range&)
      {
        LOG_ERROR("Color does not exist: %s", colorRef.c_str());
      }
    }
  }

  return textButtonStyle;
}

std::unique_ptr<ButtonStyle> Skin::parseButtonStyle(Skin* const skin, tinyxml2::XMLElement* const buttonStyleElement)
{
  std::unique_ptr<ButtonStyle> buttonStyle = std::make_unique<ButtonStyle>();
  buttonStyle->name = Utils::nullToEmpty(buttonStyleElement->Attribute("name"));

  LOG_DEBUG("Parsing button style: %s", buttonStyle->name.c_str());

  for (tinyxml2::XMLElement* style = buttonStyleElement->FirstChildElement();
       style != nullptr;
       style = style->NextSiblingElement())
  {
    if (std::string("up") == style->Name())
    {
      assignButtonStyle(style, skin->skinAtlas.get(), buttonStyle->up);
    }
    else if (std::string("over") == style->Name())
    {
      assignButtonStyle(style, skin->skinAtlas.get(), buttonStyle->over);
    }
    else if (std::string("down") == style->Name())
    {
      assignButtonStyle(style, skin->skinAtlas.get(), buttonStyle->down);
    }
    else if (std::string("checked") == style->Name())
    {
      assignButtonStyle(style, skin->skinAtlas.get(), buttonStyle->checked);
    }
    else if (std::string("disabled") == style->Name())
    {
      assignButtonStyle(style, skin->skinAtlas.get(), buttonStyle->disabled);
    }
    else
    {
      LOG_ERROR("Unknown style name %s", style->Name());
    }
  }

  return buttonStyle;
}

/*
NOTE: This method does not have validation of the skin xml file.
I'd prefer to have the skin file validated against an XSD file, but tinyxml2
doesn't support that. I might switch to xerces if I feel the need for validation.
*/
void Skin::parseSkinFile(const std::string& file)
{
  LOG_DEBUG("Parsing skin file %s", file.c_str());

  tinyxml2::XMLDocument doc;
  if (doc.LoadFile(file.c_str()))
  {
    LOG_ERROR("Could not load skin file %s: %s", file.c_str(), doc.GetErrorStr1());
    return;
  }

  tinyxml2::XMLElement* root = doc.FirstChildElement();

  std::string atlasFile = root->Attribute("atlas");
  LOG_DEBUG("Loading skin atlas: %s", atlasFile.c_str());
  skinAtlas = std::make_unique<TextureAtlas>(renderer, atlasFile);

  for (tinyxml2::XMLElement* element = root->FirstChildElement();
       element != nullptr;
       element = element->NextSiblingElement())
  {
    std::string elementName = element->Name();
    if (elementName == "colors")
    {
      LOG_DEBUG("Parsing colors...");
      parseColors(element);
    }
    else if (elementName == "fonts")
    {
      LOG_DEBUG("Parsing fonts...");
      parseFonts(element);
    }
    else if (elementName == "buttons")
    {
      LOG_DEBUG("Parsing buttons...");
      parseStyles<ButtonStyle>(element, parseButtonStyle);
    }
    else if (elementName == "textbuttons")
    {
      LOG_DEBUG("Parsing text buttons...");
      parseStyles<TextButtonStyle>(element, parseTextButtonStyle);
    }
  }
}

void resolveReferences(std::vector<tinyxml2::XMLElement*>& elements, std::map<std::string, std::size_t>& mapping)
{
  bool assigned = true;
  while (assigned)
  {
    assigned = false;
    for (std::vector<tinyxml2::XMLElement*>::iterator it = elements.begin(); it != elements.end(); )
    {
      tinyxml2::XMLElement* element = (*it);
      std::string fontRef = element->Attribute("ref");
      auto mappingIterator = mapping.find(fontRef);
      if (mappingIterator != mapping.end())
      {
        std::size_t index = (*mappingIterator).second;
        std::string name = element->Attribute("name");
        mapping[name] = index;
        it = elements.erase(it);
        assigned = true;

        LOG_DEBUG("Found reference for %s to %s", name.c_str(), fontRef.c_str());
      }
      else
      {
        it++;
      }
    }
  }
}

void Skin::parseFonts(tinyxml2::XMLElement* const fontsElement)
{
  std::vector<tinyxml2::XMLElement*> reffedFonts;

  for (tinyxml2::XMLElement* fontElement = fontsElement->FirstChildElement();
       fontElement != nullptr;
       fontElement = fontElement->NextSiblingElement())
  {
    std::string fontName = Utils::nullToEmpty(fontElement->Attribute("name"));
    std::string fontFile = Utils::nullToEmpty(fontElement->Attribute("file"));
    std::string fontRef = Utils::nullToEmpty(fontElement->Attribute("ref"));

    if (!fontRef.empty())
    {
      reffedFonts.push_back(fontElement);
      continue;
    }

    std::unique_ptr<Font> font = std::make_unique<Font>();
    if (!font->loadFromFile(fontFile))
    {
      LOG_ERROR("Error loading font: %s", fontFile.c_str());
      continue;
    }

    fonts.push_back(std::move(font));
    fontMapping[fontName] = fonts.size() - 1;
  }

  resolveReferences(reffedFonts, fontMapping);
}

void Skin::parseColors(tinyxml2::XMLElement* const colorsElement)
{
  std::vector<tinyxml2::XMLElement*> reffedColors;

  for (tinyxml2::XMLElement* colorElement = colorsElement->FirstChildElement();
       colorElement != nullptr;
       colorElement = colorElement->NextSiblingElement())
  {
    unsigned int r,g,b,a;
    std::string name = Utils::nullToEmpty(colorElement->Attribute("name"));
    std::string ref = Utils::nullToEmpty(colorElement->Attribute("ref"));
    if (!ref.empty())
    {
      reffedColors.push_back(colorElement);
      continue;
    }

    unsigned int error = 0;
    error |= colorElement->QueryUnsignedAttribute("r", &r);
    error |= colorElement->QueryUnsignedAttribute("g", &g);
    error |= colorElement->QueryUnsignedAttribute("b", &b);
    error |= colorElement->QueryUnsignedAttribute("a", &a);

    if (error != 0)
    {
      LOG_ERROR("Error converting color value to int");
      continue;
    }

    if (r > 255 || g > 255 || b > 255 || a > 255)
    {
      LOG_ERROR("Invalid color value. Must be between 0-255");
      continue;
    }

    colors.push_back({(Uint8)r,(Uint8)g,(Uint8)b,(Uint8)a});
    colorMapping[name] = colors.size() - 1;
  }

  resolveReferences(reffedColors, colorMapping);
}

} // namespace lwe