#include <LawyerEngine/UI/Button.hpp>
#include <LawyerEngine/Utils/Log.hpp>

namespace lwe
{

Button::Button(const Skin& skin, const float x, const float y)
  : Button(skin, "default", x, y)
{
}

Button::Button(const Skin& skin, const float x, const float y, const float w, const float h)
  : Button(skin, "default", x, y, w, h)
{
}

Button::Button(const Skin& skin, const std::string& styleName, const float x, const float y)
  : Widget(x, y)
{
  style = skin.getStyle<ButtonStyle>(styleName);
  setWidth((float)style->up->getRegionWidth());
  setHeight((float)style->up->getRegionHeight());
}

Button::Button(const Skin& skin, const std::string& styleName, const float x, const float y, const float w, const float h)
  : Widget(x, y, w, h)
{
  style = skin.getStyle<ButtonStyle>(styleName);
}

Button::Button(TextureRegion* const button,
               TextureRegion* const hovered,
               TextureRegion* const pressed,
               const float x,
               const float y)
  : Button(button, hovered, pressed, pressed, x, y, (float)button->getRegionWidth(), (float)button->getRegionHeight())
{
}

Button::Button(TextureRegion* const button,
               TextureRegion* const hovered,
               TextureRegion* const pressed,
               const float x,
               const float y,
               const float w,
               const float h)
  : Button(button, hovered, pressed, pressed, x, y, w, h)
{
}

Button::Button(TextureRegion* const button,
               TextureRegion* const hovered,
               TextureRegion* const pressed,
               TextureRegion* const checked,
               const float x,
               const float y)
  : Button(button, hovered, pressed, checked, button, x, y, (float)button->getRegionWidth(), (float)button->getRegionHeight())
{
}

Button::Button(TextureRegion* const button,
               TextureRegion* const hovered,
               TextureRegion* const pressed,
               TextureRegion* const checked,
               const float x,
               const float y,
               const float w,
               const float h)
  : Button(button, hovered, pressed, checked, button, x, y, w, h)
{
}

Button::Button(TextureRegion* const button,
               TextureRegion* const hovered,
               TextureRegion* const pressed,
               TextureRegion* const checked,
               TextureRegion* const disabled,
               const float x,
               const float y)
  : Button(button, hovered, pressed, checked, disabled, x, y, (float)button->getRegionWidth(), (float)button->getRegionHeight())
{
}

Button::Button(TextureRegion* const button,
               TextureRegion* const hovered,
               TextureRegion* const pressed,
               TextureRegion* const checked,
               TextureRegion* const disabled,
               const float x,
               const float y,
               const float w,
               const float h)
  : Widget(x, y, w, h)
{
  style = new ButtonStyle();
  style->up = button;
  style->over = hovered;
  style->down = pressed;
  style->checked = checked;
  style->disabled = disabled;
  styleManaged = true;
}

Button::~Button()
{
  if (styleManaged)
  {
    delete style;
  }
}

void Button::render(SDL_Renderer* const renderer)
{
  SDL_Rect destRect;
  destRect.x = (int)getX();
  destRect.y = (int)getY();
  destRect.w = (int)getWidth();
  destRect.h = (int)getHeight();

  TextureRegion* textureRegion;

  if (!this->enabled)
  {
    textureRegion = style->disabled;
  }
  else if (mousePressed && hovered)
  {
    textureRegion = style->down;
  }
  else if (keyPressed && isSelected())
  {
    textureRegion = style->down;
  }
  else if (controllerPressed && isSelected())
  {
    textureRegion = style->down;
  }
  else if (checked)
  {
    textureRegion = style->checked;
  }
  else if (isSelected() || hovered)
  {
    textureRegion = style->over;
  }
  else
  {
    textureRegion = style->up;
  }

  SDL_RenderCopy(renderer, textureRegion->getSDLTexture(), &textureRegion->getRect(), &destRect);
}

bool Button::handleEvent(const SDL_Event& ev)
{
  if (ev.type == SDL_MOUSEMOTION)
  {
    hovered = overlaps(ev.motion.x, ev.motion.y);
    // don't consume this event
    return false;
  }
  else if (!this->enabled)
  {
    return false;
  }
  else if (ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT)
  {
    mousePressed = hovered;
    return mousePressed;
  }
  else if (ev.type == SDL_MOUSEBUTTONUP && ev.button.button == SDL_BUTTON_LEFT)
  {
    if (mousePressed && hovered)
    {
      if (checkable)
      {
        checked = !checked;
      }

      if (hasAction())
      {
        performAction();
      }

      mousePressed = false;
      return true;
    }

    mousePressed = false;
  }
  else if (ev.type == SDL_KEYDOWN && selected)
  {
    if (ev.key.keysym.sym == SDLK_RETURN)
    {
      keyPressed = true;
      return true;
    }
    else if (ev.key.keysym.sym == SDLK_DOWN)
    {
      keyPressed = false;

      if (southBtn != nullptr && southBtn->isEnabled())
      {
        setSelected(false);
        southBtn->setSelected(true);
        return true;
      }
    }
    else if (ev.key.keysym.sym == SDLK_UP)
    {
      keyPressed = false;

      if (northBtn != nullptr && northBtn->isEnabled())
      {
        setSelected(false);
        northBtn->setSelected(true);
        return true;
      }
    }
    else if (ev.key.keysym.sym == SDLK_LEFT)
    {
      keyPressed = false;

      if (westBtn != nullptr && westBtn->isEnabled())
      {
        setSelected(false);
        westBtn->setSelected(true);
        return true;
      }
    }
    else if (ev.key.keysym.sym == SDLK_RIGHT)
    {
      keyPressed = false;

      if (eastBtn != nullptr && eastBtn->isEnabled())
      {
        setSelected(false);
        eastBtn->setSelected(true);
        return true;
      }
    }
  }
  else if (ev.type == SDL_KEYUP)
  {
    if (ev.key.keysym.sym == SDLK_RETURN)
    {
      if (selected && keyPressed)
      {
        if (checkable)
        {
          checked = !checked;
        }

        if (hasAction())
        {
          performAction();
        }
      
        keyPressed = false;
        return true;
      }

      keyPressed = false;
    }
  }
  else if (ev.type == SDL_CONTROLLERBUTTONDOWN && selected)
  {
    if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_A)
    {
      controllerPressed = true;
      return true;
    }
    else if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_DOWN)
    {
      controllerPressed = false;

      if (southBtn != nullptr && southBtn->isEnabled())
      {
        setSelected(false);
        southBtn->setSelected(true);
        return true;
      }
    }
    else if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_UP)
    {
      controllerPressed = false;

      if (northBtn != nullptr && northBtn->isEnabled())
      {
        setSelected(false);
        northBtn->setSelected(true);
        return true;
      }
    }
    else if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_LEFT)
    {
      controllerPressed = false;

      if (westBtn != nullptr && westBtn->isEnabled())
      {
        setSelected(false);
        westBtn->setSelected(true);
        return true;
      }
    }
    else if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_RIGHT)
    {
      controllerPressed = false;

      if (eastBtn != nullptr && eastBtn->isEnabled())
      {
        setSelected(false);
        eastBtn->setSelected(true);
        return true;
      }
    }
  }
  else if (ev.type == SDL_CONTROLLERBUTTONUP)
  {
    if (ev.cbutton.button == SDL_CONTROLLER_BUTTON_A)
    {
      if (selected && controllerPressed)
      {
        if (checkable)
        {
          checked = !checked;
        }

        if (hasAction())
        {
          performAction();
        }

        controllerPressed = false;
        return true;
      }

      controllerPressed = false;
    }
  }

  return false;
}

const std::function<void(Button* const)>& Button::getAction() const
{
  return action;
}

void Button::setAction(std::function<void(Button*)> action)
{
  this->action = action;
}

bool Button::hasAction() const
{
  return action.operator bool();
}

void Button::performAction()
{
  if (hasAction())
  {
    action(this);
  }
}

const std::shared_ptr<Button>& Button::getSouth() const
{
  return southBtn;
}

void Button::setSouth(const std::shared_ptr<Button>& south)
{
  southBtn = south;
}

const std::shared_ptr<Button>& Button::getNorth() const
{
  return northBtn;
}

void Button::setNorth(const std::shared_ptr<Button>& north)
{
  northBtn = north;
}

const std::shared_ptr<Button>& Button::getWest() const
{
  return westBtn;
}

void Button::setWest(const std::shared_ptr<Button>& west)
{
  westBtn = west;
}

const std::shared_ptr<Button>& Button::getEast() const
{
  return eastBtn;
}

void Button::setEast(const std::shared_ptr<Button>& east)
{
  eastBtn = east;
}

bool Button::isSelected() const
{
  return selected;
}

void Button::setSelected(const bool selected)
{
  this->selected = selected;
}

bool Button::isHovered() const
{
  return hovered;
}

bool Button::isPressed() const
{
  return keyPressed || mousePressed;
}

bool Button::isChecked() const
{
  return checked;
}

void Button::setChecked(bool checked)
{
  this->checked = checked;
}

bool Button::isCheckable() const
{
  return checkable;
}

void Button::setCheckable(bool checkable)
{
  this->checkable = checkable;
}

bool Button::isEnabled() const
{
  return enabled;
}

void Button::setEnabled(bool enabled)
{
  this->enabled = enabled;
}

void Button::createVerticalButtonGroup(std::initializer_list<std::shared_ptr<Button>> buttons)
{
  if (buttons.size() < 2)
  {
    return;
  }

  std::vector<std::shared_ptr<Button>> buttonVector;
  
  for (const std::shared_ptr<Button>& current : buttons)
  {
    buttonVector.push_back(current);
  }

  createVerticalButtonGroup(buttonVector);
}

void connectVertical(const std::shared_ptr<Button>& b1, const std::shared_ptr<Button>& b2)
{
  b1->setSouth(b2);
  b2->setNorth(b1);
}

void connectHorizontal(const std::shared_ptr<Button>& b1, const std::shared_ptr<Button>& b2)
{
  b1->setEast(b2);
  b2->setWest(b1);
}

void Button::createVerticalButtonGroup(const std::vector<std::shared_ptr<Button>>& buttons)
{
  if (buttons.size() < 2)
  {
    return;
  }

  // connect last and first
  connectVertical(buttons[buttons.size() - 1], buttons[0]);

  for (unsigned int i = 1; i < buttons.size(); i++)
  {
    connectVertical(buttons[i - 1], buttons[i]);
  }
}

} // namespace lwe