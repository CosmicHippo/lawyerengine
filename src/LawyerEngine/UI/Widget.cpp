#include <LawyerEngine/UI/Widget.hpp>

namespace lwe
{

Widget::Widget(const int x, const int y)
  : Widget((float)x, (float)y, 0.f, 0.f)
{
}

Widget::Widget(const int x, const int y, const int w, const int h)
  : Widget((float)x, (float)y, (float)w, (float)h)
{
}

Widget::Widget(const float x, const float y)
  : Widget(x, y, 0.f, 0.f)
{
}

Widget::Widget(const float x, const float y, const float w, const float h)
  : x(x), y(y), w(w), h(h)
{
}

void Widget::render(SDL_Renderer* const renderer)
{
}

bool Widget::handleEvent(const SDL_Event& ev)
{
  return false;
}

float Widget::getX() const
{
  return x;
}

void Widget::setX(const float x)
{
  this->x = x;
}

float Widget::getY() const
{
  return y;
}

void Widget::setY(const float y)
{
  this->y = y;
}

float Widget::getWidth() const
{
  return w;
}

void Widget::setWidth(const float w)
{
  this->w = w;
}

float Widget::getHeight() const
{
  return h;
}

void Widget::setHeight(const float h)
{
  this->h = h;
}

bool Widget::overlaps(const int x, const int y) const
{
  return overlaps((float)x, (float)y);
}

bool Widget::overlaps(const float x, const float y) const
{
  return x > this->x
      && y > this->y
      && x < this->x + w
      && y < this->y + h;
}

bool Widget::overlaps(const int x, const int y, const int w, const int h) const
{
  return overlaps((float)x, (float)y, (float)w, (float)h);
}

bool Widget::overlaps(const float x, const float y, const float w, const float h) const
{
  throw "overlaps not implemented";
}

} // namespace lwe
