#include <LawyerEngine/UI/TextButton.hpp>

namespace lwe
{

TextButton::TextButton(const Skin& skin, const std::string& text)
  : Button(skin, 0, 0), text(text)
{
}

TextButton::~TextButton()
{
}



} // namespace lwe