#include <LawyerEngine/UI/GUI.hpp>

namespace lwe
{

GUI::GUI()
{
}

GUI::~GUI()
{
}

void GUI::addWidget(const std::shared_ptr<Widget>& widget)
{
  widgets.push_back(widget);
}

void GUI::removeWidget(const std::shared_ptr<Widget>& widget)
{
  widgets.remove(widget);
}

void GUI::clearWidgets()
{
  widgets.clear();
}

bool GUI::hasWidgets() const
{
  return !widgets.empty();
}

const std::list<std::shared_ptr<Widget>>& GUI::getWidgets() const
{
  return widgets;
}

bool GUI::handleEvent(const SDL_Event& ev)
{
  for (std::shared_ptr<Widget>& widget : widgets)
  {
    if (widget->handleEvent(ev))
    {
      return true;
    }
  }

  return false;
}

void GUI::render(SDL_Renderer* const renderer)
{
  for (std::shared_ptr<Widget>& widget : widgets)
  {
    widget->render(renderer);
  }
}

} // namespace lwe