#include <LawyerEngine/Entity/Entity.hpp>
#include <iostream>

namespace lwe {

Entity::Entity()
  : Entity(0, 0, 0, 0)
{
}

Entity::Entity(const float x, const float y)
  : Entity(x, y, 0, 0)
{
}

Entity::Entity(const float x, const float y, const float w, const float h)
  : x(x), y(y), prevX(x), prevY(y), width(w), height(h)
{
}

Entity::~Entity()
{
}

void Entity::copyDataForInterpolation()
{
  prevX = getX();
  prevY = getY();
}

void Entity::render(SDL_Renderer* const renderer, const float timeAlpha)
{
}

void Entity::update(const float timeStep)
{
}

EntityManager* const Entity::getManager() const
{
  return manager;
}

float Entity::getX() const
{
  return x;
}

void Entity::setX(const float x)
{
  this->x = x;
}

float Entity::getY() const
{
  return y;
}

void Entity::setY(const float y)
{
  this->y = y;
}

float Entity::getWidth() const
{
  return width;
}

void Entity::setWidth(const float w)
{
  width = w;
}

float Entity::getHeight() const
{
  return height;
}

void Entity::setHeight(const float h)
{
  height = h;
}

float Entity::getVelocityX() const
{
  return xVel;
}

float Entity::getVelocityY() const
{
  return yVel;
}

void Entity::setVelocity(const float x, const float y)
{
  xVel = x;
  yVel = y;
}

void Entity::setVelocityX(const float x)
{
  xVel = x;
}

void Entity::setVelocityY(const float y)
{
  yVel = y;
}

float Entity::getPreviousX() const
{
  return prevX;
}

float Entity::getPreviousY() const
{
  return prevY;
}

void Entity::setPreviousX(const float x)
{
  prevX = x;
}

void Entity::setPreviousY(const float y)
{
  prevY = y;
}

bool Entity::isMoving() const
{
  return moving;
}
void Entity::setMoving(const bool moving)
{
  this->moving = moving;
}

bool Entity::isVisible() const
{
  return visible;
}
void Entity::setVisible(const bool visible)
{
  this->visible = visible;
}

bool Entity::collides(Entity* const entityA, Entity* const entityB)
{
    if (entityA->getX() + entityA->getWidth() < entityB->getX())
    {
      return false;
    }
    else if (entityB->getX() + entityB->getWidth() < entityA->getX())
    {
      return false;
    }
    else if (entityA->getY() + entityA->getHeight() < entityB->getY())
    {
      return false;
    }
    else if (entityB->getY() + entityB->getHeight() < entityA->getY())
    {
      return false;
    }

    return true;
}

float Entity::lerp(const float start, const float end, const float alpha)
{
  return start + (end - start) * alpha;
}

}