#include <LawyerEngine/Entity/EntityManager.hpp>

namespace lwe
{

EntityManager::EntityManager()
{
}

const std::vector<std::unique_ptr<Entity>>& EntityManager::getAllEntities() const
{
  return entities;
}

void EntityManager::refresh()
{
  for (std::pair<const std::size_t, std::vector<Entity*>>& pair : groupedEntities)
  {
    std::vector<Entity*>& vector(pair.second);

    vector.erase(
      std::remove_if(std::begin(vector), std::end(vector),
        [](Entity* const ptr)
        {
          return ptr->shouldBeRemoved;
        }
      ),
      std::end(vector)
    );
  }

  entities.erase(
    std::remove_if(std::begin(entities), std::end(entities),
      [](const std::unique_ptr<Entity>& ptr)
      {
        return ptr->shouldBeRemoved;
      }
    ),
    std::end(entities));
}

void EntityManager::remove(Entity* const e)
{
  e->shouldBeRemoved = true;
}

void EntityManager::clear()
{
  entities.clear();
  groupedEntities.clear();
}

} // namespace lwe