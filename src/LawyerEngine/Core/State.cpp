#include <LawyerEngine/Core/State.hpp>

namespace lwe
{

State::State()
{
}

State::~State()
{
}

void State::onEnter()
{
}

void State::onExit()
{
}

void State::onPause()
{
}

void State::onResume()
{
}

bool State::handleEvent(const SDL_Event& ev)
{
  return false;
}

void State::update(const float timeStep)
{
}

void State::render(SDL_Renderer* const renderer, const float timeAlpha)
{
}

void State::copyDataForInterpolation()
{
}

GameEngine* State::getEngine() const
{
  return engine;
}

GameSettings* State::getSettings() const
{
  return settings;
}

} // namespace lwe