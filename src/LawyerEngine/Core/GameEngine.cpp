#include <LawyerEngine/LawyerEngine.hpp>
#include <LawyerEngine/Utils/Log.hpp>
#include <sstream>
#include <algorithm>

namespace lwe {

GameEngine::GameEngine(GameSettings settings, Game* game)
  : settings(settings), game(std::unique_ptr<Game>(game))
{
  backgroundColor = {0, 0, 0, 255};
}

GameEngine::~GameEngine()
{
  for (SDL_GameController* controller : controllers)
  {
    SDL_GameControllerClose(controller);
  }
  controllers.clear();

  for (State* state : states)
  {
    LOG_DEBUG("Exiting state");
    state->onExit();
    LOG_DEBUG("Cleaning up state");
    state->cleanup();
  }

  game.reset();

  // Deleting assetManager must be done before destroying renderer
  assetManager.reset();

  if (!started)
  {
    return;
  }

  LOG_DEBUG("Quitting SDL_TTF");
  TTF_Quit();

  LOG_DEBUG("Quitting SDL_IMG");
  IMG_Quit();

  LOG_DEBUG("Destroying renderer");
  SDL_DestroyRenderer(renderer);

  // I actually want to keep the window up for as long as possible
  LOG_DEBUG("Destroying window");
  SDL_DestroyWindow(window);
  LOG_DEBUG("Quitting SDL");
  SDL_Quit();
}

bool GameEngine::init()
{
  LOG_INFO("Initializing LawyerEngine");

  if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 )
  {
    LOG_ERROR("Error initializing SDL: %s", SDL_GetError());
    return false;
  }

  window = SDL_CreateWindow(settings.WindowTitle.c_str(), 
    SDL_WINDOWPOS_UNDEFINED, 
    SDL_WINDOWPOS_UNDEFINED, 
    settings.WindowWidth, settings.WindowHeight,
    SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);

  if (window == nullptr)
  {
    LOG_ERROR("Error creating window: %s", SDL_GetError());
    return false;
  }

  LOG_DEBUG("Window with size %ix%i created.", settings.WindowWidth, settings.WindowHeight);

  renderer = SDL_CreateRenderer(window, -1, 0);
  if (renderer == nullptr)
  {
    LOG_ERROR("Error creating renderer: %s", SDL_GetError());
    return false;
  }

  if (settings.LogicalWidth > 0 && settings.LogicalHeight > 0)
  {
    SDL_RenderSetLogicalSize(renderer, settings.LogicalWidth, settings.LogicalHeight);
    LOG_DEBUG("Logical size set to %ix%i", settings.LogicalWidth, settings.LogicalHeight);
  }

  // make the scaled rendering look smoother.
  if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear"))
  {
    LOG_WARN("Linear scale hint not set.");
  }

  if (TTF_Init() < 0)
  {
    LOG_ERROR("Error initializing SDL_TTF: %s", TTF_GetError());
    return false;
  }

  if (IMG_Init(IMG_SDL_Flags) !=  IMG_SDL_Flags)
  {
    LOG_ERROR("Error initializing SDL_Image: %s", IMG_GetError());
    return false;
  }

  assetManager = std::make_unique<AssetManager>(renderer);
  return true;
}

void GameEngine::pushState(State* const state)
{
  if (!states.empty() && state == states.back())
  {
    return;
  }

  pendingOnEnterStates.push_back(state);

  if (!isInitialized(state))
  {
    pendingInitStates.push_back(state);
  }

  pendingPushStates.push_back(state);
}

State* GameEngine::popState()
{
  return popState(true);
}

State* GameEngine::popState(const bool cleanup)
{
  if (states.empty())
  {
    return nullptr;
  }

  State* const poppedState = states.back();
  states.pop_back();

  pendingOnExitStates.push_back(poppedState);

  if (cleanup)
  {
    pendingCleanupStates.push_back(poppedState);
  }

  if (!states.empty())
  {
    pendingOnEnterStates.push_back(states.back());
  }

  return poppedState;
}

void GameEngine::setState(State* const state)
{
  setState(state, true);
}

void GameEngine::setState(State* const state, const bool cleanUp)
{
  for (unsigned int i = 0; i < states.size(); i++)
  {
    popState(cleanUp);
  }
  
  pushState(state);
}

bool GameEngine::handleEvent(const SDL_Event& ev)
{
  if (ev.type == SDL_QUIT)
  {
    exit();
    return true;
  }

  if (ev.type == SDL_CONTROLLERDEVICEADDED)
  {
    SDL_GameController* controller = SDL_GameControllerOpen(ev.cdevice.which);
    if (controller != nullptr)
    {
      LOG_INFO("Opened game controller %s(%i)", SDL_GameControllerName(controller), ev.cdevice.which);
      controllerDeviceToControllerMap[ev.cdevice.which] = controller;
      controllerControllerToDeviceMap[controller] = ev.cdevice.which;
      controllers.push_back(controller);
    }
    else
    {
      LOG_ERROR("Error opening game controller %s(%i): %s", SDL_GameControllerName(controller), ev.cdevice.which, SDL_GetError());
    }
    return true;
  }
  else if (ev.type == SDL_CONTROLLERDEVICEREMOVED)
  {
    LOG_INFO("ControllerDeviceRemoved: %i", ev.cdevice.which);
    SDL_GameController*& controller = controllerDeviceToControllerMap[ev.cdevice.which];
    controllerDeviceToControllerMap.erase(ev.cdevice.which);
    controllerControllerToDeviceMap.erase(controller);
    controllers.remove(controller);

    return true;
  }
  else if (ev.type == SDL_CONTROLLERDEVICEREMAPPED)
  {
    LOG_INFO("ControllerDeviceRemapped: %i", ev.cdevice.which);
    return true;
  }

  return false;
}

bool GameEngine::isInitialized(State* const state)
{
  return std::find(initializedStates.begin(), initializedStates.end(), state) != initializedStates.end();
}

void GameEngine::manageStates()
{
  for (State* const state : pendingOnExitStates)
  {
    LOG_DEBUG("Exiting state");
    state->onExit();
  }
  pendingOnExitStates.clear();

  for (State* const state : pendingCleanupStates)
  {
    LOG_DEBUG("Cleaning up state");
    state->cleanup();
    initializedStates.erase(state);
  }
  pendingCleanupStates.clear();

  for (State* const state : pendingInitStates)
  {
    LOG_DEBUG("Initializing state");
    state->game = game.get();
    state->engine = this;
    state->settings = &settings;

    if (!state->init())
    {
      LOG_ERROR("State couldn't be initialized.");
      state->cleanup();
      state->game = nullptr;
      state->engine = nullptr;
      state->settings = nullptr;
      std::remove(pendingPushStates.begin(), pendingPushStates.end(), state);
      std::remove(pendingOnEnterStates.begin(), pendingOnEnterStates.end(), state);
    }
    else
    {
      initializedStates.insert(state);
    }
  }
  pendingInitStates.clear();

  for (State* const state : pendingPushStates)
  {
    states.push_back(state);
  }
  pendingPushStates.clear();

  for (State* const state : pendingOnEnterStates)
  {
    LOG_DEBUG("Entering state");
    state->onEnter();
  }
  pendingOnEnterStates.clear();
}

int GameEngine::run()
{
  if (running)
  {
    return -1;
  }

  started = true;
  running = true;

  if (!init())
  {
    LOG_ERROR("Engine initialization failed.");
    return -1;
  }

  this->game->engine = this;
  this->game->init();
  
  if (pendingPushStates.empty())
  {
    LOG_ERROR("No State set. Exiting ...");
    return -1;
  }

  float accumulator = 0;
  SDL_Event ev;
  long lastTime = SDL_GetTicks();

  Uint32 TICKS_PER_FRAME = (Uint32)(1000.f / settings.MaxFPS);

  while (running)
  {

    float deltaTime = ((SDL_GetTicks() - lastTime) / 1000.f) * getGameSpeed();
    // TODO: limit deltaTime to avoid spiral of death
    
    lastTime = SDL_GetTicks();
    accumulator += deltaTime;


    ///// STATE MANAGEMENT /////
    manageStates();

    if (states.empty())
    {
      exit();
      break;
    }

    State* const currentState = states.back();
    //////////////////////////



    ///// EVENT HANDLING /////
    while (SDL_PollEvent(&ev))
    {
      // The engine handles events first.
      if (handleEvent(ev))
      {
        continue;
      }

      if (currentState->handleEvent(ev))
      {
        continue;
      }
    }
    if (!running) break; // If any event handling made the game exit
    //////////////////////////



    ///// TICKING THE GAME /////
    if (accumulator >= settings.TimeStep) {
#ifdef WIN32
      std::stringstream ss;
      ss << settings.WindowTitle << " - FPS: " << fps.getFPS();
      SDL_SetWindowTitle(window, ss.str().c_str());
#endif
    }

    int loops = 0;
    while (accumulator >= settings.TimeStep && loops < settings.MaxFrameSkip)
    { 
      // there is no need to copy data for interpolation if another update is happening before the render
      if (accumulator < settings.TimeStep * 2 && loops <= settings.MaxFrameSkip)
      {
        for (State* const state : states)
        {
          state->copyDataForInterpolation();
        }
      }

      currentState->update(settings.TimeStep);
      accumulator -= settings.TimeStep;
      loops++;
    }
    ////////////////////////////



    ///// RENDERING /////
    // clear the screen before rendering
    SDL_SetRenderDrawColor(renderer, backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
    SDL_RenderClear(renderer);

    // for rendering interpolation
    float alpha = accumulator / settings.TimeStep;

    // render all states in reverse order
    for (State* const state : states)
    {
      state->render(renderer, alpha);
    }
    /////////////////////

    SDL_RenderPresent(renderer);

    fps.update();

    // Cap frame rate if MAX_FPS is set
    if (settings.MaxFPS > 0) {
      Uint32 ticks = SDL_GetTicks() - lastTime;
      if (ticks < TICKS_PER_FRAME) {
        SDL_Delay(TICKS_PER_FRAME - ticks);
      }
    }
  } // main loop

  return 0;
}

void GameEngine::exit()
{
  running = false;
}

const GameSettings& GameEngine::getSettings() const
{
  return settings;
}

AssetManager* GameEngine::getAssetManager() const
{
  return assetManager.get();
}

SDL_Renderer* GameEngine::getRenderer() const
{
  return renderer;
}

SDL_Window* GameEngine::getWindow() const
{
  return window;
}

int GameEngine::getWindowWidth() const
{
  int w;
  SDL_GetWindowSize(window, &w, NULL);
  return w;
}

int GameEngine::getWindowHeight() const
{
  int h;
  SDL_GetWindowSize(window, NULL, &h);
  return h;
}

float GameEngine::getGameSpeed() const
{
  return gameSpeed;
}

void GameEngine::setGameSpeed(const float gameSpeed)
{
  this->gameSpeed = gameSpeed;
}

const SDL_Color& GameEngine::getBackgroundColor() const
{
  return backgroundColor;
}

void GameEngine::setBackgroundColor(const SDL_Color& color)
{
  backgroundColor = color;
}

const std::list<SDL_GameController*>& GameEngine::getControllers() const
{
  return controllers;
}

Sint32 GameEngine::getControllerDeviceID(SDL_GameController* controller) const
{
  try
  {
    return controllerControllerToDeviceMap.at(controller);
  }
  catch (std::out_of_range&)
  {
    return -1;
  }
}

} // namespace lwe