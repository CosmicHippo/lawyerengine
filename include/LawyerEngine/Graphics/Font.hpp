#ifndef LWE_FONT_HPP
#define LWE_FONT_HPP

#include <LawyerEngine/Config.hpp>

#include <SDL_ttf.h>
#include <unordered_map>

namespace lwe {

class LW_API_EXPORT Font
{
public:
  Font() {}
  virtual ~Font();

  bool loadFromFile(std::string fileName);

  TTF_Font* getTTF_Font(int size);
  std::string getFileName() const;
private:
  bool loaded{ false };
  TTF_Font* loadSize(int size);

  std::unordered_map<int, TTF_Font*> m_fonts;
  std::string m_fileName;
};

} // namespace lwe

#endif