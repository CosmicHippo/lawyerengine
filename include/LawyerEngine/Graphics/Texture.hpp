#ifndef LWE_TEXTURE_HPP
#define LWE_TEXTURE_HPP

#include <LawyerEngine/Config.hpp>

#include <SDL.h>
#include <string>

namespace lwe {

class LW_API_EXPORT Texture
{
public:
  Texture(SDL_Texture* texture);
  ~Texture();

  int getWidth() const;
  int getHeight() const;
  Uint32 getFormat() const;
  int getAccess() const;

  SDL_Texture* getSDLTexture() const;

  static Texture* createTexture(SDL_Renderer* renderer, const std::string& fileName);

private:
  int width;
  int height;

  int access;
  Uint32 format;

  SDL_Texture* texture;
};

} // namespace lwe

#endif