#ifndef LWE_TEXTUREREGION_HPP
#define LWE_TEXTUREREGION_HPP

#include <LawyerEngine/Config.hpp>

#include <LawyerEngine/Graphics/Texture.hpp>

namespace lwe {

class LW_API_EXPORT TextureRegion
{
public:
  TextureRegion();
  TextureRegion(const TextureRegion& region);
  TextureRegion(Texture* texture);
  TextureRegion(Texture* texture, int width, int height);
  TextureRegion(Texture* texture, int x, int y, int width, int height);
  virtual ~TextureRegion();

  void setRegion(Texture* texture);
  void setRegion(TextureRegion* textureRegion);
  void setRegion(const TextureRegion& textureRegion);
  void setRegion(int x, int y, int width, int height);

  int getRegionX() const;
  int getRegionY() const;
  int getRegionWidth() const;
  int getRegionHeight() const;

  const SDL_Rect& getRect(); //lol
  Texture* getTexture() const;
  SDL_Texture* getSDLTexture() const;

private:
  Texture* const texture{ NULL };
  SDL_Rect regionRect;

  int regionX{ 0 };
  int regionY{ 0 };
  int regionWidth{ 0 };
  int regionHeight{ 0 };
};

} // namespace lwe

#endif