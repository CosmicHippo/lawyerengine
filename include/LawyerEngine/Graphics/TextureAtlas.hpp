#ifndef LWE_TEXTUREATLAS_HPP
#define LWE_TEXTUREATLAS_HPP

#include <LawyerEngine/Config.hpp>

#include <LawyerEngine/Graphics/TextureRegion.hpp>
#include <LawyerEngine/Graphics/Texture.hpp>

#include <string>
#include <vector>
#include <memory>

namespace lwe {

class AtlasRegion : public TextureRegion
{

friend class TextureAtlas;

public:
  AtlasRegion() {}
  virtual ~AtlasRegion();
  std::string getName() const;
  int getIndex() const;

private:
  AtlasRegion(const std::string& name, int index, Texture* t, int x, int y, int w, int h);
  std::string name;
  int index;
};



class LW_API_EXPORT TextureAtlas
{
public:
  TextureAtlas(SDL_Renderer* renderer, const std::string& _file);
  ~TextureAtlas();

  TextureRegion* findRegion(const std::string& name) const;
  TextureRegion* findRegion(const std::string& name, int index) const;
  std::vector<TextureRegion*> findRegions(const std::string& name) const;

private:
  std::vector<AtlasRegion*> regions;
  std::vector<Texture*> textures;

  void load();

  SDL_Renderer* renderer;

  std::string m_fileName;
};

} // namespace lwe

#endif