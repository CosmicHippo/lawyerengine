#ifndef LWE_TEXT_HPP
#define LWE_TEXT_HPP

#include <LawyerEngine/Config.hpp>

#include <SDL_ttf.h>

#include <LawyerEngine/Graphics/Texture.hpp>
#include <LawyerEngine/Graphics/Font.hpp>

namespace lwe {

class LW_API_EXPORT Text
{
public:
  Text(SDL_Renderer* renderer, Font* font, std::string text, int fontSize, float x, float y, const SDL_Color& color);
  Text(SDL_Renderer* renderer, Font* font, std::string text, int fontSize, float x, float y, int r, int g, int b);
  virtual ~Text();

  void render();

  void updateText(int _number);
  void updateText(const std::string& _newText);

  const SDL_Color& getColor() const;
  void setColor(const SDL_Color& color);
  void setColor(int r, int g, int b);
  void setColor(float r, float g, float b);

  float getX() const;
  void setX(float x);

  float getY() const;
  void setY(float y);

  float getWidth() const;
  float getHeight() const;

  bool isVisible() const;
  void setVisible(bool visible);

  void setFontSize(int size);
  int getFontSize() const;

  void ensureTextUpdated();
private:
  bool needsUpdate{ true };

  float m_x{ 0 };
  float m_y{ 0 };
  float m_width{ 0 };
  float m_height{ 0 };
  int m_fontSize{ 0 };
  SDL_Color m_color;
  bool m_visible{ true };
  Font* m_font{ NULL };
  std::string m_text{ "" };

  Texture* m_texture{ NULL };
  SDL_Renderer* m_renderer{ NULL };
};

} // namespace lwe

#endif 
