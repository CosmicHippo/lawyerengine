#ifndef LWE_ENTITY_HPP
#define LWE_ENTITY_HPP

#include <SDL.h>
#include <LawyerEngine/Config.hpp>
#include <LawyerEngine/Entity/EntityManager.hpp>

#include <string>
#include <vector>

namespace lwe
{

/**
 * A base entity.
 */
class LW_API_EXPORT Entity
{

friend class EntityManager;

public:
  Entity();
  Entity(float x, float y);
  Entity(float x, float y, float w, float h);
  virtual ~Entity();

  void copyDataForInterpolation();

  virtual void render(SDL_Renderer* renderer, float timeAlpha);
  virtual void update(float timeStep);

  float getX() const;
  float getY() const;
  void setX(float x);
  void setY(float y);

  float getWidth() const;
  float getHeight() const;
  void setWidth(float w);
  void setHeight(float h);

  float getVelocityX() const;
  float getVelocityY() const;
  void setVelocityX(float x);
  void setVelocityY(float y);
  void setVelocity(float x, float y);

  float getPreviousX() const;
  float getPreviousY() const;

  void setPreviousX(float x);
  void setPreviousY(float y);

  bool isMoving() const;
  void setMoving(bool moving);

  bool isVisible() const;
  void setVisible(bool visible);

  static bool collides(Entity* entityA, Entity* entityB);

protected:
  EntityManager* const getManager() const;
  float lerp(float start, float end, float alpha);

private:
  EntityManager* manager{ NULL };

  bool visible{ true };
  float x{ 0 };
  float y{ 0 };
  float width{ 0 };
  float height{ 0 };

  float xVel{ 0 };
  float yVel{ 0 };
  float prevX{ 0 };
  float prevY{ 0 };
  bool moving{ false };

  bool shouldBeRemoved{ false };
};

}

#endif
