#ifndef LWE_ENTITYMANAGER_HPP
#define LWE_ENTITYMANAGER_HPP

#include <LawyerEngine/Config.hpp>
#include <LawyerEngine/Entity/Entity.hpp>

#include <typeinfo>
#include <memory>
#include <map>
#include <vector>
#include <algorithm>


namespace lwe
{

class Entity;

/**
 * An entity manager manages entities, but handling creation/destruction and fetching of
 * created entities, grouped by entity type.
 */
class LW_API_EXPORT EntityManager
{
public:
  EntityManager();

  /**
   * Creates a new entity. The arguments are passed to the corresponding constructor of the chosen entity.
   *
   * @param mArgs the arguments to pass to the constructor of the entity.
   * @return a pointer to a newly created entity.
   */
  template<typename T, typename... TArgs> T* create(TArgs&&... mArgs)
  {
    static_assert(std::is_base_of<Entity, T>::value, "T must be base of Entity");

    std::unique_ptr<T> unique_ptr(std::make_unique<T>(std::forward<TArgs>(mArgs)...));
    T* ptr(unique_ptr.get());

    groupedEntities[typeid(T).hash_code()].push_back(ptr);
    entities.push_back(std::move(unique_ptr));

    ptr->manager = this;
    return ptr;
  }

  /**
   * Returns a reference to the vector containing pointers to all entities of a given type
   * managed by this manager.
   *
   * @return a reference to the vector containing pointers to all entities of a given type.
   */
  template<typename T> const std::vector<Entity*>& getAll()
  {
    return groupedEntities[typeid(T).hash_code()];
  }

  /**
   * Returns a reference to the vector containing the unique pointers of all entities.
   *
   * @return a reference to the vector containing the unique pointers of all entities.
   */
  const std::vector<std::unique_ptr<Entity>>& getAllEntities() const;

  /**
   * Flags an entity for removal from this manager. The entity will be deleted upon the next
   * call to refresh().
   *
   * @param entity the entity to remove.
   */
  void remove(Entity* entity);

  /**
   * Refreshes the state of the manager by removing entities that has been flagged for removal.
   * This method should be called in the beginning of an update phase.
   */
  void refresh();

  /**
   * Flags all managed entities for removal.
   */
  void clear();

private:
  std::vector<std::unique_ptr<Entity>> entities;
  std::map<std::size_t, std::vector<Entity*>> groupedEntities;

/*
These need to be added because there is a vector with unique_ptr in this class.
Using __declspec(dllexport), the compiler defines copy constructor and copy
assignment operators, which will in turn call the copy constructor of m_entities.
*/
  EntityManager(const EntityManager&) = delete;
  EntityManager& operator=(const EntityManager&) = delete;
};

} // namespace lwe

#endif