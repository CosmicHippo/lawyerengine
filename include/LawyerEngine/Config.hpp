#ifndef LW_CONFIG_H
#define LW_CONFIG_H

#ifdef _WIN32
  #define LW_API_EXPORT __declspec(dllexport)

  #ifdef _MSC_VER
    #pragma warning(disable: 4251)
  #endif
#else // Linux, Mac
  #define LW_API_EXPORT
#endif

#endif