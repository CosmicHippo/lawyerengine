#ifndef LWE_SKIN_HPP
#define LWE_SKIN_HPP

#include <LawyerEngine/Graphics/TextureAtlas.hpp>
#include <LawyerEngine/Graphics/TextureRegion.hpp>
#include <LawyerEngine/Graphics/Font.hpp>
#include <LawyerEngine/Utils/Log.hpp>
#include <LawyerEngine/UI/Style.hpp>

#include <TinyXML/tinyxml2.h>

#include <memory>
#include <map>
#include <functional>

namespace lwe
{

class LW_API_EXPORT Skin
{
public:
  Skin(SDL_Renderer* renderer, const std::string& skinFile);
  virtual ~Skin();

  template<typename T>
  T* getStyle(const std::string& name) const
  {
    try
    {
      std::map<std::string, Style*> map = groupedResources.at(typeid(T).hash_code());

      try
      {
        T* pointer = reinterpret_cast<T*>(map.at(name));
        if (pointer == nullptr)
        {
          LOG_ERROR("Error casting Style to T: %s", name.c_str());
        }

        return pointer;
      }
      catch (std::out_of_range&)
      {
        LOG_ERROR("Resource not found: %s", name.c_str());
        return nullptr;
      }
    }
    catch (std::out_of_range&)
    {
      LOG_ERROR("Resource type for %s not present.", name.c_str());
      return nullptr;
    }
  }

private:
  template<typename T>
  void putResource(std::unique_ptr<T>& resource)
  {
    static_assert(std::is_base_of<Style, T>::value, "T must be base of Style");
    if (resource.get() == nullptr)
    {
      return;
    }

    if (groupedResources.find(typeid(T).hash_code()) == groupedResources.end())
    {
      groupedResources[typeid(T).hash_code()] = std::map<std::string, Style*>();
    }

    std::map<std::string, Style*>& map = groupedResources[typeid(T).hash_code()];

    if (map.find(resource->name) == map.end())
    {
      map[resource->name] = resource.get();
      resources.push_back(std::move(resource));
    }
  }

  template<typename T>
  void parseStyles(tinyxml2::XMLElement* elements, std::function<std::unique_ptr<T>(Skin*, tinyxml2::XMLElement*)> parseMethod)
  {
    static_assert(std::is_base_of<Style, T>::value, "T must be base of Style");

    for (tinyxml2::XMLElement* element = elements->FirstChildElement();
         element != nullptr;
         element = element->NextSiblingElement())
    {
      std::unique_ptr<T> style = parseMethod(this, element);
      putResource<T>(style);
    }
  }

  void parseSkinFile(const std::string& file);

  void parseFonts(tinyxml2::XMLElement* element);
  void parseColors(tinyxml2::XMLElement* element);

  // must be static, or else they can't be referenced as function pointers
  static std::unique_ptr<ButtonStyle> parseButtonStyle(Skin* self, tinyxml2::XMLElement* element);
  static std::unique_ptr<TextButtonStyle> parseTextButtonStyle(Skin* self, tinyxml2::XMLElement* element);

  /* Properties */

  SDL_Renderer* renderer;

  std::vector<std::unique_ptr<Style>> resources;
  std::map<std::size_t, std::map<std::string, Style*>> groupedResources;
  std::unique_ptr<TextureAtlas> skinAtlas;

  std::vector<std::unique_ptr<Font>> fonts;
  std::map<std::string, std::size_t> fontMapping; // maps names to indices in 'fonts'
  std::vector<SDL_Color> colors;
  std::map<std::string, std::size_t> colorMapping; // maps names to indices in 'colors'

  Skin(const Skin& copy) = delete;
  Skin& operator=(const Skin& copy) = delete;
};

} // namespace lwe

#endif