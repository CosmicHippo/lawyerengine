#ifndef LW_BUTTON_HPP
#define LW_BUTTON_HPP

#include <LawyerEngine/Config.hpp>
#include <LawyerEngine/Graphics/TextureRegion.hpp>
#include <LawyerEngine/UI/Skin.hpp>
#include <LawyerEngine/UI/Widget.hpp>
#include <LawyerEngine/UI/Style.hpp>

#include <SDL.h>

#include <vector>
#include <functional>

namespace lwe
{

class Skin;

class LW_API_EXPORT Button : public Widget
{
public:
  Button(const Skin& skin, float x, float y);
  Button(const Skin& skin, float x, float y, float w, float h);
  Button(const Skin& skin, const std::string& styleName, float x, float y);
  Button(const Skin& skin, const std::string& styleName, float x, float y, float w, float h);

  Button(TextureRegion* button,
         TextureRegion* hovered,
         TextureRegion* pressed,
         float x,
         float y);

  Button(TextureRegion* button,
         TextureRegion* hovered,
         TextureRegion* pressed,
         float x,
         float y,
         float w,
         float h);

  Button(TextureRegion* button,
         TextureRegion* hovered,
         TextureRegion* pressed,
         TextureRegion* checked,
         float x,
         float y);

  Button(TextureRegion* button,
         TextureRegion* hovered,
         TextureRegion* pressed,
         TextureRegion* checked,
         float x,
         float y,
         float w,
         float h);

  Button(TextureRegion* button,
         TextureRegion* hovered,
         TextureRegion* pressed,
         TextureRegion* checked,
         TextureRegion* disabled,
         float x,
         float y);

  Button(TextureRegion* button,
         TextureRegion* hovered,
         TextureRegion* pressed,
         TextureRegion* checked,
         TextureRegion* disabled,
         float x,
         float y,
         float w,
         float h);

  virtual ~Button();

  void render(SDL_Renderer* renderer) override;

  const std::function<void(Button*)>& getAction() const;

  void setAction(std::function<void(Button*)> action);
  void performAction();
  bool hasAction() const;

  bool handleEvent(const SDL_Event&) override;

  const std::shared_ptr<Button>& getSouth() const;
  void setSouth(const std::shared_ptr<Button>& btn);

  const std::shared_ptr<Button>& getNorth() const;
  void setNorth(const std::shared_ptr<Button>& btn);

  const std::shared_ptr<Button>& getWest() const;
  void setWest(const std::shared_ptr<Button>& btn);

  const std::shared_ptr<Button>& getEast() const;
  void setEast(const std::shared_ptr<Button>& btn);

  bool isSelected() const;
  void setSelected(bool selected);

  bool isChecked() const;
  void setChecked(bool checked);

  bool isCheckable() const;
  void setCheckable(bool checkable);

  bool isEnabled() const;
  void setEnabled(bool enabled);

  bool isHovered() const;
  bool isPressed() const;

  static void createVerticalButtonGroup(std::initializer_list<std::shared_ptr<Button>> buttons);
  static void createVerticalButtonGroup(const std::vector<std::shared_ptr<Button>>& buttons);

private:
  // true if this button should delete the style upon being deleted.
  bool styleManaged { false };
  ButtonStyle* style;

  std::function<void(Button*)> action;

  std::shared_ptr<Button> southBtn { nullptr };
  std::shared_ptr<Button> northBtn { nullptr };
  std::shared_ptr<Button> westBtn { nullptr };
  std::shared_ptr<Button> eastBtn { nullptr };

  bool checkable { false };
  bool enabled { true };
  
  bool checked { false };
  bool selected { false };
  bool hovered { false };
  bool mousePressed { false };
  bool keyPressed { false };
  bool controllerPressed { false };
};

} // namespace lwe
#endif