#ifndef LWE_STYLE_HPP
#define LWE_STYLE_HPP

#include <string>

#include <LawyerEngine/Graphics/TextureRegion.hpp>
#include <LawyerEngine/Graphics/Font.hpp>

#include <SDL.h>

namespace lwe
{

struct Style
{
  std::string name;
};

struct ButtonStyle : public Style
{
  TextureRegion* up { nullptr };
  TextureRegion* over { nullptr };
  TextureRegion* down { nullptr };
  TextureRegion* checked { nullptr };
  TextureRegion* disabled { nullptr };
};

struct TextButtonStyle : public ButtonStyle
{
  lwe::Font* font;
  SDL_Color color;
};


}

#endif