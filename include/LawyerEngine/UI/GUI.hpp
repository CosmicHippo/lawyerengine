#ifndef LWE_GUI_HPP
#define LWE_GUI_HPP

#include <SDL.h>

#include <LawyerEngine/UI/Widget.hpp>

#include <list>

namespace lwe
{

class LW_API_EXPORT GUI
{
public:
  GUI();
  virtual ~GUI();

  bool handleEvent(const SDL_Event& ev);
  void render(SDL_Renderer* renderer);

  void addWidget(const std::shared_ptr<Widget>& widget);
  void removeWidget(const std::shared_ptr<Widget>& widget);
  bool hasWidgets() const;
  void clearWidgets();

  const std::list<std::shared_ptr<Widget>>& getWidgets() const;

private:
  std::list<std::shared_ptr<Widget>> widgets;

  GUI(const GUI& copy) = delete;
  GUI& operator=(const GUI& copy) = delete;
};

} // namespace lwe

#endif