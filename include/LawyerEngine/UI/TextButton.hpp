#ifndef LWE_TEXTBUTTON_HPP
#define LWE_TEXTBUTTON_HPP

#include <LawyerEngine/UI/Button.hpp>
#include <LawyerEngine/UI/Style.hpp>

namespace lwe
{

class TextButton : public Button
{
public:
  TextButton(const Skin& skin, const std::string& text);
  virtual ~TextButton();

  const std::string& getText() const;
  void setText(const std::string& text);

  int getFontSize() const;
  void setFontSize(int size);

  const Font& getFont() const;
  void setFont(const Font& font);

private:
  TextButtonStyle style;
  std::string text;
  Font* font;
  int fontSize;
};

} // namespace lwe

#endif