#ifndef LWE_WIDGET_HPP
#define LWE_WIDGET_HPP

#include <LawyerEngine/UI/Skin.hpp>

namespace lwe
{

class LW_API_EXPORT Widget
{
public:
  Widget(int x, int y);
  Widget(int x, int y, int w, int h);

  Widget(float x, float y);
  Widget(float x, float y, float w, float h);

  virtual void render(SDL_Renderer* renderer);
  virtual bool handleEvent(const SDL_Event& ev);

  float getX() const;
  void setX(float x);

  float getY() const;
  void setY(float y);

  float getWidth() const;
  void setWidth(float w);

  float getHeight() const;
  void setHeight(float h);

  bool overlaps(int x, int y) const;
  bool overlaps(float x, float y) const;

  bool overlaps(int x, int y, int w, int h) const;
  bool overlaps(float x, float y, float w, float h) const;

private:
  float x { 0 };
  float y { 0 };
  float w { 0 };
  float h { 0 };
};

} // namespace lwe

#endif