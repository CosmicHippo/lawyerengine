#include <LawyerEngine/Utils/StringUtils.hpp>
#include <LawyerEngine/Utils/ArrayUtils.hpp>

#include <SDL.h>

namespace lwe
{

namespace Utils
{

extern LW_API_EXPORT SDL_Color getContrastColor(const SDL_Color& color);

} // namespace Utils

} // namespace lwe