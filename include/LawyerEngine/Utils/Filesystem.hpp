#ifndef LW_FILESYSTEM_H
#define LW_FILESYSTEM_H

#include <string>
#include <LawyerEngine/Config.hpp>

namespace lwe
{
namespace filesys 
{
  /**
   *  Gets the current working directory
   *
   *  @return Full path to the current working directory, empty string if anything went wrong.
   */
  LW_API_EXPORT std::string cwd();

  /**
   *  Get the execution directory, same as get_cwd() if not in windows.
   *
   *  @return Full path to the current execution directory
   */
  LW_API_EXPORT std::string executionDirectory();
  
  /**
   *  Checks if a file exists.
   *
   *  @param name Full path to a file
   *  @return True if the file is found, False otherwise
   */
  LW_API_EXPORT bool fileExists(const std::string& name);

} // namespace lwe::filesys
} // namespace lwe

#endif
