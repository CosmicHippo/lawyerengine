#ifndef LWE_ARRAYUTILS_HPP
#define LWE_ARRAYUTILS_HPP

#include <string>
#include <sstream>
#include <vector>

namespace lwe
{

namespace Utils
{

extern std::vector<std::string>& split(const std::string &s, char delim, std::vector<std::string>& elems);

extern std::vector<std::string> split(const std::string &s, char delim);

} // namespace Utils

} // namespace lwe

#endif