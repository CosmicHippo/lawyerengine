#ifndef LW_FPS_H
#define LW_FPS_H

#include <SDL.h>

class FPS
{
public:
  FPS() {}
  ~FPS() {}

  void update();
  int getFPS();

private:
  unsigned int oldTime{ 0 };
  unsigned int frames{ 0 };
};

#endif
