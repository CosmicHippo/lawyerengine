#ifndef LOG_H
#define LOG_H

#include <LawyerEngine/Config.hpp>

namespace lwe
{

enum LoggingLevel
{
  LEVEL_TRACE = 1,
  LEVEL_DEBUG,
  LEVEL_INFO,
  LEVEL_WARN,
  LEVEL_ERROR
};

class LW_API_EXPORT Log
{
public:
  Log();
  ~Log() {}

  void log(LoggingLevel level, const char* func, int line, const char* msg, ...);
  static Log& getLogger();

  LoggingLevel getLoggingLevel() const { return currentLevel; }
  void setLoggingLevel(LoggingLevel level);

private:
  LoggingLevel currentLevel { LEVEL_ERROR };
  static Log logger;
};


#if defined(WIN32)
#define __func__ __FUNCTION__
#endif

#define LOG(level, msg, ...) \
  if (level < lwe::Log::getLogger().getLoggingLevel()) ; \
  else lwe::Log::getLogger().log(level, __func__, __LINE__, msg, __VA_ARGS__)

#define LOG_TRACE(msg, ...) LOG(lwe::LEVEL_TRACE, msg, __VA_ARGS__)
#define LOG_DEBUG(msg, ...) LOG(lwe::LEVEL_DEBUG, msg, __VA_ARGS__)
#define LOG_INFO(msg, ...) LOG(lwe::LEVEL_INFO, msg, __VA_ARGS__)
#define LOG_WARN(msg, ...) LOG(lwe::LEVEL_WARN, msg, __VA_ARGS__)
#define LOG_ERROR(msg, ...) LOG(lwe::LEVEL_ERROR, msg, __VA_ARGS__)

}

#endif //LOG_H
