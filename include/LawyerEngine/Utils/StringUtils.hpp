#ifndef LWE_STRINGUTILS_HPP
#define LWE_STRINGUTILS_HPP

#include <LawyerEngine/Config.hpp>
#include <string>

namespace lwe
{

namespace Utils
{

extern LW_API_EXPORT bool endsWith(const std::string& string, const std::string& suffix);

extern LW_API_EXPORT bool startsWith(const std::string& string, const std::string& prefix);

extern LW_API_EXPORT std::string nullToEmpty(const char* const str);

extern LW_API_EXPORT std::string getFileExtension(const std::string& fileName);

extern LW_API_EXPORT std::string getFileWithoutExtension(const std::string& fileName);

} // namespace Utils

} // namespace lwe

#endif