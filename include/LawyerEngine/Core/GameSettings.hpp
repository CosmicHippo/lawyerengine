#ifndef LW_GAMESETTINGS_H
#define LW_GAMESETTINGS_H

#include <string>

namespace lwe
{

/**
 * Game settings class used to create a GameEngine.
 *
 * TODO: Should be a struct?
 */
class LW_API_EXPORT GameSettings
{
public:
  std::string WindowTitle{ "LawyerEngine" };
  int WindowWidth{ 800 };
  int WindowHeight{ 600 };
  int LogicalWidth { -1 };
  int LogicalHeight { -1 };
  int MaxFPS{ 200 };

  float TimeStep{ 1 / 60.f };
  int MaxFrameSkip{ 10 };

  bool EntityInterpolationEnabled{ true };
};

} // namespace lwe

#endif