#ifndef LW_STATE_H
#define LW_STATE_H

#include <LawyerEngine/Config.hpp>

#include <LawyerEngine/Core/GameEngine.hpp>
#include <LawyerEngine/Core/Game.hpp>
#include <LawyerEngine/Core/GameSettings.hpp>

#include <SDL.h>

namespace lwe
{

/**
 * Base interface for States in a game. The Game Engine manages the state handling
 * in a controlled manner. State changing happens at the beginning of a new update loop.
 */
class LW_API_EXPORT State
{
friend class GameEngine;

public:
  virtual ~State();

  /**
   * Initializes the state. Will be called when the state is entered the first time,
   * and all subsequent times if the state is cleaned up when left.
   *
   * @return true if the initialization was successful, false otherwise.
   */
  virtual bool init() = 0;

  /**
   * Cleans up the state. Should reset it so that init() can be called without problems
   * the next time the state is entered.
   */
  virtual void cleanup() = 0;

  /**
   * Called when the state exits. Not the same as cleanup, since onExit can be called
   * when a new state is entered, but this state is chosen to not be cleaned up.
   */
  virtual void onExit();

  /**
   * Called when the state gets entered. This method is called after init(), if init() is
   * called.
   */
  virtual void onEnter();

  /**
   * Not called yet from the engine.
   */
  virtual void onPause();

  /**
   * Not called yet from the engine.
   */
  virtual void onResume();

  /**
   * Handles events that's passed from the engine. Should return true if the event is to be
   * consumed, and not processed by subsequent event handlers.
   *
   * @return true if the event is consumed.
   */
  virtual bool handleEvent(const SDL_Event& ev);

  /**
   * Updates the state. The timeStep parameter passed is not neccessarily the actual time since
   * the last update, but the update method should simulate that it is. The engine tries to keep
   * tick the game at a consistent speed, so the timeStep variable is highly dependent on the
   * settings that the engine was created with. See the constructor for the GameEngine.
   *
   * @param timeStep time since the last update in seconds. 
   */
  virtual void update(float timeStep);

  /**
   * Renders the state.
   *
   * If multiple renders happens between updates, entity position might not have changed. TimeAlpha
   * is used to interpolate the position of entities if that occurs.
   *
   * @param renderer A pointer to the SDL_Renderer to use.
   * @param timeAlpha A value between 0-1. Used for interpolation of entities.
   */
  virtual void render(SDL_Renderer* renderer, float timeAlpha);

  /**
   * Called by the engine to indicate that entities should copy data for interpolation. The Entity
   * base class already has this implemented, but the engine doesn't manage the entities itself, so
   * the state has to call copyDataForInterpolation() on all entities itself.
   */
  virtual void copyDataForInterpolation();

  /**
   * Returns a pointer to the Game that this state is bound to. Templated to prevent casting.
   *
   * @return a pointer to the Game that this state is bound to.
   */
  template<class T>
  T* getGame() const
  {
    static_assert(std::is_base_of<Game, T>::value, "T must be base of Game");
    return reinterpret_cast<T*>(game);
  }

  /**
   * Returns a pointer to the engine that this state was added to.
   *
   * @return a pointer to the engine that this state was added to.
   */
  GameEngine* getEngine() const;

  /**
   * Returns a pointer to the settings that the GameEngine was created with.
   *
   * @return a pointer to the settings that the GameEngine was created with.
   */
  GameSettings* getSettings() const;

protected:
  State();

private:
  Game* game;
  GameEngine* engine;
  GameSettings* settings;
};

} // namespace lwe
#endif