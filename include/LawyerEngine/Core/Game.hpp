#ifndef LW_GAME_H
#define LW_GAME_H

#include <LawyerEngine/Config.hpp>

namespace lwe
{

class GameEngine;

/**
 * An abstract base class for a Game.
 */
class LW_API_EXPORT Game
{

// friended so that the engine member variable can be set when the game is run
friend class GameEngine;

public:
  Game() {}
  virtual ~Game() {}

  /**
   * Initializes the Game.
   *
   * TODO: Make this return a bool.
   */
  virtual void init() = 0;

  /**
   * Returns a pointer to the game engine that started this game.
   *
   * @return a pointer to the game engine that started this game.
   */
  GameEngine* getEngine() const { return engine; }

private:
  GameEngine* engine;
};

} // namespace lwe

#endif