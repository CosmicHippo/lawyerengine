#ifndef _GAMEENGINE_H_
#define _GAMEENGINE_H_

#include <LawyerEngine/Config.hpp>

#include <LawyerEngine/Core/GameSettings.hpp>
#include <LawyerEngine/Core/State.hpp>
#include <LawyerEngine/Core/Game.hpp>

#include <LawyerEngine/Assets/AssetManager.hpp>

#include <LawyerEngine/Utils/FPS.hpp>
#include <SDL.h>
#include <SDL_image.h>

#include <vector>
#include <list>
#include <set>
#include <memory>

//extern "C" {
//#include <lua.h>
//#include <lauxlib.h>
//#include <lualib.h>
//}

namespace lwe 
{

class State;

/**
 * The Game Engine class.
 */
class LW_API_EXPORT GameEngine
{
public:
  /**
   * Creates a new GameEngine.
   *
   * @param settings The GameSettings to create the engine with.
   * @param game a pointer to a Game. The engine takes control of the game instance,
   *             so do not call delete on the passed game.
   */
  GameEngine(GameSettings settings, Game* game);
  ~GameEngine();

//  lua_State* LuaState;

  /**
   * Sets the next state to enter. This will cause all current states to be exited and cleaned up.
   *
   * @param state A pointer to the state to be set.
   */
  void setState(State* state);

  /**
   * Sets the next state to enter.
   *
   * @param state A pointer to the state to be set.
   * @param cleanup if true, the current states will be cleaned up.
   */
  void setState(State* state, bool cleanup);

  /**
   * Pushes a state to the state stack. The pushed state will be the state that recieves input,
   * while all other states will be rendered in the reverse order.
   */
  void pushState(State* state);

  /**
   * Pops the state stack.
   *
   * @param cleanup whether the state should be cleaned up or not. If false, the state will not
   *                be initialized when it's entered the next time.
   * @return a pointer to the popped state. nullptr if no state was set.
   */
  State* popState(bool cleanup);

  /**
   * Calls popState(true)
   */
  State* popState();

  /**
   * Starts the game that the engine was created with.
   *
   * @return an exit code. 0 if everything went ok.
   */
  int run();

  /**
   * Exits the game at the beginning of the next frame.
   */
  void exit();

  /**
   * Returns a pointer to the Game that the engine is running. Returns nullptr if the
   * engine hasn't started a game yet.
   *
   * @return a pointer to the Game that the engine is running.
   */
  template<class T>
  T* getGame() const
  {
    static_assert(std::is_base_of<Game, T>::value, "T must be base of Game");
    return reinterpret_cast<T*>(game.get());
  }

  /**
   * Returns a pointer to the GameSettings that the engine was created with.
   *
   * @return a pointer to the GameSettings that the engine was created with.
   */
  const GameSettings& getSettings() const;

  /**
   * Returns a pointer to the AssetManager.
   *
   * @return a pointer to the AssetManager.
   */
  AssetManager* getAssetManager() const;

  /**
   * Returns a pointer to the current SDL_Renderer.
   *
   * @return a pointer to the current SDL_Renderer.
   */
  SDL_Renderer* getRenderer() const;

  /**
   * Returns a pointer to the current SDL_Window.
   *
   * @return a pointer to the current SDL_Window.
   */
  SDL_Window* getWindow() const;

  /**
   * Returns the width of the current window in pixels.
   *
   * @return the width of the current window in pixels.
   */
  int getWindowWidth() const;

  /**
   * Returns the height of the current window in pixels.
   *
   * @return the height of the current window in pixels.
   */
  int getWindowHeight() const;

  /**
   * Returns the current game speed.
   *
   * @return the current game speed.
   */
  float getGameSpeed() const;

  /**
   * Sets the game speed.
   *
   * @param gameSpeed the game speed to be set.
   */
  void setGameSpeed(float gameSpeed);

  const SDL_Color& getBackgroundColor() const;

  void setBackgroundColor(const SDL_Color& color);

  const std::list<SDL_GameController*>& getControllers() const;
  Sint32 getControllerDeviceID(SDL_GameController* controller) const;

private:
  bool init();
  bool handleEvent(const SDL_Event&);

  GameSettings settings;
  std::unique_ptr<Game> game;
  FPS fps;
  SDL_Window* window{ NULL };
  SDL_Renderer* renderer{ NULL };

  std::map<Sint32, SDL_GameController*> controllerDeviceToControllerMap;
  std::map<SDL_GameController*, Sint32> controllerControllerToDeviceMap;
  std::list<SDL_GameController*> controllers;

  std::unique_ptr<AssetManager> assetManager;

  SDL_Color backgroundColor;

  bool running{ false };
  bool started{ false };
  float gameSpeed{ 1.f };

  void manageStates();
  std::vector<State*> states;
  std::vector<State*> pendingOnEnterStates;
  std::vector<State*> pendingOnExitStates;
  std::vector<State*> pendingCleanupStates;
  std::vector<State*> pendingInitStates;
  std::vector<State*> pendingPushStates;

  /**
   * Returns true if the state is initialized. 
   *
   * @param state the state to check for.
   * @return true if the state is initialized.
   */
  bool isInitialized(State* state);
  std::set<State*> initializedStates;

  // TODO make these configurable
  const int IMG_SDL_Flags { IMG_INIT_JPG | IMG_INIT_PNG };
};

} // namespace lwe
#endif
