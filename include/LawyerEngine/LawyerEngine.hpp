#ifndef LAWYERENGINE_HPP
#define LAWYERENGINE_HPP

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include <LawyerEngine/Config.hpp>
#include <LawyerEngine/Utils/Log.hpp>

#include <LawyerEngine/Core/GameEngine.hpp>
#include <LawyerEngine/Core/Game.hpp>
#include <LawyerEngine/Core/GameSettings.hpp>
#include <LawyerEngine/Core/State.hpp>

#include <LawyerEngine/Assets/AssetManager.hpp>

#include <LawyerEngine/Graphics/Font.hpp>
#include <LawyerEngine/Graphics/Text.hpp>
#include <LawyerEngine/Graphics/Texture.hpp>
#include <LawyerEngine/Graphics/TextureRegion.hpp>
#include <LawyerEngine/Graphics/TextureAtlas.hpp>

#include <LawyerEngine/Entity/Entity.hpp>
#include <LawyerEngine/Entity/EntityManager.hpp>

#include <LawyerEngine/Event/EventCondition.hpp>
#include <LawyerEngine/Event/Trigger.hpp>

#include <LawyerEngine/Utils/Filesystem.hpp>
#include <LawyerEngine/Utils/FPS.hpp>
#include <LawyerEngine/Utils/Utils.hpp>

#include <LawyerEngine/UI/GUI.hpp>
#include <LawyerEngine/UI/Skin.hpp>
#include <LawyerEngine/UI/Button.hpp>
#include <LawyerEngine/UI/TextButton.hpp>

#endif