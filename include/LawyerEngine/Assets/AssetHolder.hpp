#ifndef LWE_ASSETHOLDER_HPP
#define LWE_ASSETHOLDER_HPP

#include <LawyerEngine/Utils/Log.hpp>
#include <unordered_map>
#include <memory>

namespace lwe {

template<typename T> class AssetHolder
{
public:
  void put(const std::string& key, T* value)
  {
    if (has(key))
    {
      LOG_ERROR("Asset replaced in asset holder: %s", key.c_str());
    }

    assets[key] = std::shared_ptr<T>(value);
  }

  T* get(const std::string& key)
  {
    try
    {
      return assets.at(key).get();
    }
    catch (const std::out_of_range&)
    {
      return nullptr;
    }
  }

  bool has(const std::string& key)
  {
    return assets.find(key) != assets.end();
  }

private:
  std::unordered_map<std::string, std::shared_ptr<T>> assets;
};

} // namespace lwe

#endif