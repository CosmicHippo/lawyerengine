#ifndef LW_ASSETMANAGER_H
#define LW_ASSETMANAGER_H

#include <LawyerEngine/Config.hpp>

#include <LawyerEngine/Graphics/TextureAtlas.hpp>
#include <LawyerEngine/Graphics/Texture.hpp>
#include <LawyerEngine/UI/Skin.hpp>
#include <LawyerEngine/Assets/AssetHolder.hpp>

#include <SDL.h>

#include <string>

namespace lwe {

class LW_API_EXPORT AssetManager
{
public:
  AssetManager(SDL_Renderer* renderer) : renderer(renderer) {}
  virtual ~AssetManager() {}

  template<typename T> bool load(const std::string& fileName);

  /**
   * Returns a loaded asset. Currently only supports lwe::Texture and lwe::TextureAtlas.
   *
   * @return a loaded asset.
   */
  template<typename T> T* get(const std::string& fileName);

private:
  SDL_Renderer* const renderer;

  AssetHolder<Texture> textures;
  AssetHolder<TextureAtlas> textureAtlases;
  AssetHolder<Skin> skins;
};

// LOAD ----------------------------------------------------------------

template<> inline bool AssetManager::load<Texture>(const std::string& fileName)
{
  if (textures.has(fileName))
  {
    return false;
  }

  Texture* t = Texture::createTexture(renderer, fileName);

  if (t == nullptr)
  {
    return false;
  }

  textures.put(fileName, t);

  return true;
}

template<> inline bool AssetManager::load<TextureAtlas>(const std::string& fileName)
{
  if (textureAtlases.has(fileName))
  {
    return false;
  }

  TextureAtlas* t = new TextureAtlas(renderer, fileName);

  if (t == nullptr)
  {
    return false;
  }
  
  textureAtlases.put(fileName, t);

  return true;
}

template<> inline bool AssetManager::load<Skin>(const std::string& fileName)
{
  if (skins.has(fileName))
  {
    return false;
  }

  Skin* skin = new Skin(renderer, fileName);

  if (skin == nullptr)
  {
    return false;
  }

  skins.put(fileName, skin);

  return true;
}

// /LOAD ----------------------------------------------------------------

// GET -----------------------------------------------------------------

template<> inline Texture* AssetManager::get<Texture>(const std::string& fileName)
{
  return textures.get(fileName);
}

template<> inline TextureAtlas* AssetManager::get<TextureAtlas>(const std::string& fileName)
{
  return textureAtlases.get(fileName);
}

template<> inline Skin* AssetManager::get<Skin>(const std::string& fileName)
{
  return skins.get(fileName);
}

// /GET -----------------------------------------------------------------


} // namespace lwe

#endif