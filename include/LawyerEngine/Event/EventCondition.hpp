#ifndef LWE_EVENTCONDITION_HPP
#define LWE_EVENTCONDITION_HPP

#include <SDL.h>

#include <vector>
#include <memory>

#include <LawyerEngine/Config.hpp>
#include <LawyerEngine/Event/Trigger.hpp>

namespace lwe
{

/**
 * An EventCondition is used to batch triggers from various inputs into a single object. Using EventConditions,
 * it's easier to manage event handling keybindings for actions.
 *
 * The following example demonstrates how to bind both the up arrow key on the keyboard and the up button on
 * the game controller as a single condition.
 *
 * Example initialization(i.e. in a constructor):
 * <pre>
 * EventCondition moveForward;
 *
 * moveForward.addTrigger(new KeyboardTrigger(SDLK_UP));
 * moveForward.addTrigger(new GameControllerButtonTrigger(SDL_CONTROLLER_BUTTON_DPAD_UP));
 * </pre>
 *
 * Example usage in an event handling method
 * <pre>
 * bool handleEvent(const SDL_Event& ev)
 * {
 *   if (moveForward(ev))
 *   {
 *     // do something
 *     return true;
 *   }
 *
 *   return false;
 * }
 * </pre>
 */
class LW_API_EXPORT EventCondition
{
public:
  EventCondition();
  virtual ~EventCondition();

  /**
   * Returns true if the event is consumed.
   *
   * @param ev the SDL_Event reference
   * @return true if the event is consumed.
   */
  bool operator()(const SDL_Event& ev) const;

  /**
   * Returns true if the event is consumed.
   * Result will be true if the conditions is met.
   *
   * An event may be consumed without having the conditions met if the condition is a series
   * of events. I.e. a key press contains two actions: keydown + keyup. The keydown event
   * will consume the event, but the conditions will not be met, since the conditions require
   * a keyup event to follow. The following keyup event will consume the event, and the passed
   * result bool will be set to true.
   *
   * Example usage of this method:
   * <pre>
   * bool result = false;
   * if (eventCondition(sdlEvent, &result))
   * {
   *   if (result)
   *   {
   *     // condition is met. perform an action.
   *   }
   *
   *   // condition is not yet met, but the event is consumed.
   *   return true;
   * }
   * </pre>
   *
   * @param ev the SDL_Event reference
   * @param result a pointer to a bool that will be set to true if the conditions are met.
   * @return true if the event is consumed.
   */
  bool operator()(const SDL_Event& ev, bool* result) const;

  /**
   * Adds a trigger to this condition.
   *
   * @param trigger a shared pointer to a Trigger that will be added to this condition.
   */
  void addTrigger(const std::shared_ptr<Trigger>& trigger);

  /**
   * Returns true if this condition has any triggers.
   *
   * @return true if this condition has any triggers.
   */
  bool hasTriggers() const;

private:
  std::vector<std::shared_ptr<Trigger>> triggers;
};

} // namespace lwe

#endif