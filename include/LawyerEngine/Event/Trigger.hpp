#ifndef LWE_TRIGGER_HPP
#define LWE_TRIGGER_HPP

#include <SDL.h>

#include <LawyerEngine/Config.hpp>

namespace lwe
{

/**
 * A Trigger catches SDL_Events.
 *
 * Example usage:
 * <pre>
 * Trigger trigger;
 * bool result = false;
 * if (trigger(event, &result))
 * {
 *   if (result)
 *   {
 *     // trigger conditions met!
 *   }
 *
 *   return true; // consume event!
 * }
 *
 * return false;
 * </pre>
 */
class LW_API_EXPORT Trigger
{
public:
  Trigger();
  virtual ~Trigger();

  /**
   * Returns true if the event is to be consumed. Whether the trigger conditions is met
   * is determined by the result bool.
   *
   * @param ev the SDL_Event to check this trigger with.
   * @param result a pointer to the result. This value will be set to true if the trigger conditions is met.
   * @return true if the event is to be consumed.
   */
  virtual bool operator()(const SDL_Event& ev, bool* result) = 0;
};

/**
 * A Trigger that listens on the keyboard. A standard keyboard trigger is the result of
 * a key being pressed down, followed by the key being released. The trigger sets the result
 * to true on the KEYUP event.
 */
class LW_API_EXPORT KeyboardTrigger : public Trigger
{
public:
  /**
   * Creates a trigger that listens for the given key.
   *
   * @param key the SDL_Keycode to listen for.
   */
  KeyboardTrigger(SDL_Keycode key);

  /**
   * Creates a trigger that listen for a given key.
   *
   * @param key the SDL_Keycode to listen for.
   * @param onDown if true, the trigger will set result to true on the KEYDOWN event.
   */
  KeyboardTrigger(SDL_Keycode key, bool onDown);
  virtual ~KeyboardTrigger();

  bool operator()(const SDL_Event& ev, bool* result) override;

private:
  bool pressed;
  bool onDown{ false };
  SDL_Keycode key{ 0 };
};

/**
 * A Trigger that listens on the game controller buttons. Similarly to the KeyboardTrigger,
 * a standard GameControllerButtonTrigger listens for a button being pressed down, followed
 * by the button being released. The trigger sets the result to true on the BUTTONUP event.
 */
class LW_API_EXPORT GameControllerButtonTrigger : public Trigger
{
public:
  GameControllerButtonTrigger(SDL_GameControllerButton button);
  GameControllerButtonTrigger(SDL_GameControllerButton button, bool onDown);
  /**
   * Creates a trigger that listens for the given button. Sets result to true on the
   * BUTTONUP event.
   *
   * @param device the device ID
   * @param button the button to listen for.
   */
  GameControllerButtonTrigger(Sint32 device, SDL_GameControllerButton button);

  /**
   * Creates a trigger that listens for the given button. If onDown is true, the result
   * will be set to true already on the BUTTONDOWN event.
   *
   * @param device the device ID
   * @param button the button to listen for.
   * @param onDown if true, the trigger will be true for the BUTTONDOWN event.
   */
  GameControllerButtonTrigger(Sint32 device, SDL_GameControllerButton button, bool onDown);
  virtual ~GameControllerButtonTrigger();

  bool operator()(const SDL_Event& ev, bool* result) override;

private:
  bool isCorrectDevice(Sint32 device) const;

  bool pressed { false };
  bool onDown { false };
  Sint32 device { -1 };
  SDL_GameControllerButton button;
};

} // namespace lwe

#endif