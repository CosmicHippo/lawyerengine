LawyerEngine
===

---

LawyerEngine aims to be a simple 2D engine using SDL 2.0 as its backbone.

---

Libraries needed:
---

 - [SDL 2.0](https://www.libsdl.org/download-2.0.php)
 - [SDL_image 2.0](https://www.libsdl.org/projects/SDL_image/)
 - [SDL_ttf 2.0](https://www.libsdl.org/projects/SDL_ttf/)
 
---

Some Enviroment Variables are needed for windows development

 - SDL2_PATH
 - SDL2_IMAGE_PATH
 - SDL2_TTF_PATH

How to change environment variables:

Windows:

	Start Menu -> Right click on Computer -> Properties
	Click Advanced System Settings -> Environment Variables
	
Linux:

	export SDL2_PATH /path/to/sdl2
